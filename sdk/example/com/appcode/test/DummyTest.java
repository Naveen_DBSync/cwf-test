package com.appcode.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.internal.runners.statements.Fail;

import com.appcode.test.MyDummyService.MyFilter;
import com.appcode.test.MyDummyService.MyItem;
import com.appcode.test.MyDummyService.MyReturn;
import com.appmashups.appcode.SaveResult;

public class DummyTest {

	@Test
	public void testReadMyFirst() {

		try {

			MyDummyService svc = new MyDummyService();

			System.out.println("*** Running READ Operation ***");
			
			svc.open();

			MyFilter filter = new MyFilter();
			filter.name = "Rajeev";

			List<MyReturn> returnList = svc.readMyFirst(filter);

			for (MyReturn ret : returnList) {

				System.out.println("Name:" + ret.name + "Email:" + ret.email);

			}
			
			svc.close();
			
			System.out.println("Success");
			
		} catch (Exception e) {
			e.printStackTrace();
			//$FALL-THROUGH$
			assert(false);
		}
	}
	
	@Test
	public void testWriteMyFirst() {

		try {

			MyDummyService svc = new MyDummyService();

			System.out.println("*** Running WRITE Operation ***");
			
			svc.open();
			
			MyReturn myreturn = new MyReturn();
			myreturn.email = "samxx@avankia.com";
			myreturn.id = 1;
			myreturn.name ="sam1";
			myreturn.myItemList = new ArrayList<MyDummyService.MyItem>();
			myreturn.myItemList.add(new MyItem("item1", 1));
			
			MyReturn myreturn2 = new MyReturn();
			myreturn2.email = "rajyy@avankia.com";
			myreturn2.id = 1;
			myreturn2.name ="raj2";
			myreturn2.myItemList = new ArrayList<MyDummyService.MyItem>();
			myreturn2.myItemList.add(new MyItem("item2", 1));
			
			List<MyReturn> returnList = new ArrayList<MyDummyService.MyReturn>();
			
			returnList.add(myreturn);
			returnList.add(myreturn2);

			SaveResult[] results = svc.writeMyFirst(returnList);

			for (SaveResult result : results) {

				System.out.println("id:" + result.id + "New Id:" + result.newid);

			}
			
			svc.close();
			
			System.out.println("Success");
			
		} catch (Exception e) {
			e.printStackTrace();
			//$FALL-THROUGH$
			assert(false);
		}
	}

}
