package com.appcode.test;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import com.appmashups.appcode.AppCode;
import com.appmashups.appcode.Context;
import com.appmashups.appcode.ErrorInformation;
import com.appmashups.appcode.SaveResult;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MyDummyService implements AppCode {

	private boolean hasRun = false;

	static Logger logger = Logger.getLogger(MyDummyService.class.getName());

	public static class MyFilter {
		public String name;
		public String url;
		public String nothing = "nothing";
	}

	public static class MyReturn {
		public String name;
		public int id;
		public String email = "default";
		public List<MyItem> myItemList = new ArrayList<MyDummyService.MyItem>();
		public MyItem[] myItemList2 = new MyItem[] {};

		public MyReturn() {
		}

		public MyReturn(String name, int id) {
			this.name = name;
			this.id = id;
		}
	}

	public static class MyItem {
		public String itemname;
		public int id;

		public MyItem() {
		}

		public MyItem(String name, int id) {
			this.itemname = name;
			this.id = id;
		}
	}

	@Override
	public void setContext(Context ctx) {
		// TODO Auto-generated method stub

	}

	@Override
	public void open() throws RemoteException {
		// TODO Auto-generated method stub

	}

	@Override
	public void close() throws RemoteException {
		// TODO Auto-generated method stub

	}

	List<MyReturn> readMyFirst(MyFilter filter) {

		logger.log(Level.INFO, "MyDummyService.readMyFirst Called: for Query:"
				+ filter.name);

		try {

			if (!hasRun) {

				if (filter != null)
					System.out.println("Received Query:" + filter.name);

				ArrayList<MyReturn> retList = new ArrayList<MyReturn>();

				MyReturn return1 = new MyReturn("Rajeev", 1);
				return1.myItemList.add(new MyItem("Test1", 11));

				MyReturn return2 = new MyReturn("Sambit", 2);
				return2.myItemList2 = new MyItem[2];
				return2.myItemList2[0] = new MyItem("Test2", 12);
				return2.myItemList2[1] = new MyItem("Test3", 13);

				if (filter.name != null) {

					if (filter.name != null && filter.name.equals("Rajeev")) {

						retList.add(return1);
					}

					if (filter.name != null && filter.name.equals("Sambit")) {

						retList.add(return2);

					}

				} else {

					retList.add(return1);
					retList.add(return2);
				}

				hasRun = true;

				return retList;

			} else {

				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE,
					"Error while executing method MyDummyService:readMyFirst",
					e);
		}

		return null;
	}

	SaveResult[] writeMyFirst(List<MyReturn> dataList) {
		ArrayList<SaveResult> retList = new ArrayList<SaveResult>();
		int count = 0;

		if (dataList != null && dataList.size() > 0) {
			for (MyReturn r : dataList) {
				count++;

				System.out.println("Processing Data:" + r.name + "," + r.id);

				SaveResult sr = new SaveResult();
				sr.id = "" + count;
				sr.newid = "" + count;
				sr.success = true;

				System.out.println("SaveResult:" + sr.newid);

				retList.add(sr);
			}

		}else {
			//Create an error for SaveResult
			SaveResult errorResult = new SaveResult();
			ErrorInformation[] errorInfoArr = {new ErrorInformation("ERR01", 
																	"No Input Data Passed" , "writeMyFirst", "")};
			errorResult.errors = errorInfoArr;
			
			retList.add(errorResult);
		}

		return retList.toArray(new SaveResult[] {});
	}

}
