package myappcode.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import myappcode.HelloWorldSvc;
import myappcode.HelloWorldSvc.HelloFilter;
import myappcode.HelloWorldSvc.HelloParameter;

import org.apache.tools.ant.taskdefs.Concat;
import org.junit.Test;

import com.appmashups.appcode.Context;

public class TestHelloWorldAppCode {


	//@Test
	public void testOpen() {

		try {

			HelloWorldSvc svc = new HelloWorldSvc();

			svc.setContext(null);

			svc.open();

			svc.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

	//@Test
	public void testReadHello() {
	
		try {

			HelloWorldSvc svc = new HelloWorldSvc();

			svc.setContext(null);

			svc.open();
			
			HelloFilter filter = new HelloFilter();
			filter.key = "Scott";
			
			svc.readHello(filter);

			svc.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	
	
	}

	@Test
	public void testWriteHello() {
		try {

			HelloWorldSvc svc = new HelloWorldSvc();

			svc.setContext(null);

			svc.open();
	
			HelloParameter param = new HelloParameter();
			param.name = "Test1";
			param.id = "id1";
			
			
			HelloParameter param2 = new HelloParameter();
			param2.name = "Test1";
			param2.id = "id2";
			
			List<HelloParameter> inputParams = new ArrayList<HelloWorldSvc.HelloParameter>();
			inputParams.add(param);
			inputParams.add(param2);
			
			svc.writeHello(inputParams);
			
			svc.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}

}
