var errorMsg = '';
function clearValidated() {
	$("#valBtnTxt").text("Validate Connection");
	$("#valBtnTxt").removeClass("btn-primary");
	$("#valBtnTxt").addClass("btn-warning");
	$("#saveBtnTxt").text("Save");
	
	//for Odata connector connect button
	if($("#oauthConnect").length) {
		$("#oauthConnect").text("Connect");
		$("#oauthConnect").addClass("btn-success");
		$("#oauthConnect").removeClass("btn-primary");
	}
	
	$("#errorBox").hide();
}

function clearActivated() {
	$("#activateBtn").text("Activate");
	$("#activateBtn").removeClass("btn-primary");
	$("#activateBtn").addClass("btn-danger");
	$("#activateBtn").removeAttr("style");
}

jQuery(document).ready(function(){
	jQuery("input").on("input", clearValidated);
	jQuery("select").on("change", clearValidated);
});

function deleteConnector(project, selectedAdapter) {
	errorMsg = '';
	
	$.ajax({
		type : "GET",
		url : 'getAdapterReferences',
		data : "project="+ project,
		cache : false,
		success : function(adapterReferences) {
				
			if(Object.keys(adapterReferences[selectedAdapter]).length > 0) {
				var referred = "";

				for (var i in adapterReferences[selectedAdapter]) {
					
					var processName = i;
					referred += "<b>" + processName + " : </b>";
					var wfJson = (adapterReferences[selectedAdapter])[i];
					
					for(var k in wfJson) {
						var wf = wfJson[k];
						referred += " " + wf + ",";
					}
	  
					if(referred.endsWith(",")) {
						referred = referred.substring(0, referred.length-1) + "<br/>";			    	
					}

					swal({
						type: "error",
						title: "Can't delete connector",
						text: "<div style=\"text-align: left;margin-left: 60px;word-break: break-word;\">It is referred in following Workflows :<div style=\"overflow-y: auto;max-height: 100px;\">" + referred + "</div></div>",
						html: true
					});
				}
			} 
			// Delete the adapter, as there are no references in the Project
			else {
				swal({
					title : "Delete Connector?",
					text : "Your will not be able to recover connector "+ selectedAdapter +" !",
					//type: "warning",
					showCancelButton : true,
					confirmButtonColor : "#DD6B55",
					confirmButtonText : "Yes, delete it!",
					cancelButtonText : "No, cancel it!",
					closeOnConfirm : true,
					closeOnCancel : true
				}, function(isConfirm) {
					if (isConfirm) {

						$.ajax({
							type : "GET",
							url : 'deleteAdapter',
							data : "project="+ project +"&adapter="+selectedAdapter,
							cache : false,
							success : function(msg) {
								if(msg.status == "success") {
									window.location.href = "setup?project="+project;
								} else {
									errorMsg = "Error while Deleting, Please try again";
								}
								
							}, error : function(xhr, status, error) {
								if (xhr.status == 200) {
									errorMsg = "Session expired. Please login once again";
								} else {
									errorMsg = "There seem to be some technical problem. Please try saving again";
								}
							}
						});
						
						if (errorMsg != '') {
							$("#errorBox").show();
							$("#errorContent").html(errorMsg);
						}
					}
				});
			}

		}, error : function(xhr, status, error) {
			if (xhr.status == 200) {
				errorMsg = "Session expired. Please login once again";
			} else {
				errorMsg = "There seem to be some technical problem. Please try saving again";
			}
		}
	});
}

function saveConnector(project, selectedAdapter) {
	$("#errorBox").hide();
	errorMsg = '';
	$.ajax({
		type : "POST",
		url : "saveAdapter",
		data : "project=" + project + "&adapter=" + selectedAdapter + "&"
				+ $("#connectorForm").serialize(),
		cache : false,
		success : function(data) {
			if(data.status == "success") {
				$("#saveBtnTxt").text("Saved Successfully");
				$("#errorBox").hide();
				
				var curl_link = $("#r").val();
				if(typeof curl_link !== "undefined") {
					window.location.href = curl_link + "openAdapter?project="+ project + "&adapter=" + selectedAdapter + "&r=t";
				}
				
				var endpoint = $("#prop_endpoint").val();
				if(endpoint && endpoint.indexOf("dynamics.com") > 0) {
					$("#activationButtonId").show();
				} else {
					$("#activationButtonId").hide();
				}
			}
			else {
				errorMsg = "Error while Saving, Please try again";
			}
			
			if (errorMsg != '') {
				$("#errorBox").show();
				$("#errorContent").html(errorMsg);
			}
		},
		error : function(xhr, status, error) {
			if (xhr.status == 200) {
				errorMsg = "Session expired. Please login once again";
			} else {
				errorMsg = "There seem to be some technical problem. Please try saving again";
			}
			
			if (errorMsg != '') {
				$("#errorBox").show();
				$("#errorContent").html(errorMsg);
			}
		}
	});
}

function validateConnectorProperties(project, selectedAdapter, currentAdapterType) {
	$("#errorBox").hide();
	$("#valBtnTxt").text("Validating...");
	errorMsg = '';
	$.ajax({
		type : "POST",
		url : 'validateConnector',
		data : "project="+ project +"&adapter="+selectedAdapter +"&"
		+ $("#connectorForm").serialize(),
		cache : false,
		success : function(msg) {
			msg = $.trim(msg);
			if (msg == "Connection Success") {
				$("#valBtnTxt").removeClass("btn-warning");
				$("#valBtnTxt").addClass("btn-primary");
				$("#valBtnTxt").text("Successfully Validated");
				$("#errorBox").hide();
			} else {
				if (currentAdapterType == "SalesforceAdapter") {
					errorMsg = "Login Failed: Invalid Username, password or security token.<br/><br/>Please Check:<br/>1. Username / Password is correct.<br/>2. Your security token is entered correctly. To enable security token go to: <br/>&emsp;'Your Name'->Setup->My Personal Information->Reset My Security Token OR <br/>&emsp;'Your Name'->My Settings->Personal->Reset My Security Token";
				} else {
					errorMsg = msg;
				}
				clearActivated();
				clearValidated();
			}
			
			if (errorMsg != '') {
				$("#errorBox").show();
				$("#errorContent").html(errorMsg);
			}
		},
		error : function(xhr, status, error) {
			$("#valBtnTxt").text("Validate Connection");
			if (xhr.status == 200) {
				errorMsg = "Session expired. Please login once again";
			} else {
				errorMsg = "There seem to be some technical problem. Please try validating again";
			}
			
			if (errorMsg != '') {
				$("#errorBox").show();
				$("#errorContent").html(errorMsg);
			}
		}
	});
}

function create_adapter(project, newAdapterName, adapterType) {
	
	errorMsg = '';
	
	$.ajax({
		type : "GET",
		url : "createAdapter",
		data : "project="+ project +"&adapter="+ newAdapterName +"&adapterType="+ adapterType,
		cache : false,
		success : function(data) {
			if(data.status == "success") {
				window.location.href = "openAdapter?project="+ project +"&adapter="+ newAdapterName;
			}
			else {
				errorMsg = "Error while creating, Please try again";
			}
			
			if (errorMsg.trim() != '') {
				$("#cNameId").addClass("error");
				$("#errors").text(errorMsg);
				$("#errors").show();
			}
		},
		error : function(xhr, status, error) {
			if (xhr.status == 200) {
				errorMsg = "Session expired. Please login once again";
			} else {
				errorMsg = "There seem to be some technical problem. Please try saving again";
			}
			
			if (errorMsg.trim() != '') {
				$("#cNameId").addClass("error");
				$("#errors").text(errorMsg);
				$("#errors").show();
			}
		}
	});
}