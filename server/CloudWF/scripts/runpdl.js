$(document).ready(function() {
	$(".runpdl-btn").on("click", runpdl);
	$(".project-list").on("click", ".viewConsole", runpdl);
	$(".runpdl-console").on("click", ".stopBox", stopProcess);
});

var isViewConsoleRunning = false;

function getConsoleHTML(curl_link, process){
	if($(".runpdl-console").find("#consoleResult").length){
		return;
	}
	$.ajax({
		type: "GET",
		url: curl_link + "consoleView?process=" + process,               
		cache: false,
		async: false
	}).done(function(data) {
		$(".runpdl-console").append(data);
	});
}

//This function is used to run the process
var runpdl = function run_pdl() {
	var pdlName = $(this).data("process");
	process_RPL = $(this).data("process");
	profileName_RPL = $(this).data("profilename");
	curl_link_RPL = $(this).data("curl_link");
	logIndex = 0;
	getConsoleHTML(curl_link_RPL, process_RPL);
	pdlName = "processdefinition_" + pdlName + ".xml";
	var run_url='runpdl.m?profileName='+profileName_RPL+'&processFileName='+pdlName+'&command=start';
	var mytime= "&ms="+new Date().getTime();
	run_url += mytime;
	$.ajax({
		type: "GET",
		url: curl_link_RPL + run_url,               
		cache:false,
		dataType : "json"
	}).done(function(data) {
		$("#consoleResult").show();
		$(window).scrollTop($('#consoleBox').offset().top);
		if(isViewConsoleRunning == false){
			run_viewConsole();
		}
	});
}

var stopProcess = function stopProcess() {
	//var process_STP = $(this).data("process");
	//var profileName_SPL = $(this).data("profilename");
	//var curl_link = $(this).data("curl_link");
	var pdlName = "processdefinition_" + process_RPL + ".xml";
	$.ajax({
		type: "GET",
		url: curl_link + "/log/kill?profileName="+profileName_RPL+"&processFileName="+pdlName,               
		cache:false,
		dataType : "json"
	})
	.done(function(data) {
		alert("Process Stopped!!!");
	});
}

var viewConsole = function viewConsole() {
	process_RPL = $(this).data("process");
	profileName_RPL = $(this).data("profilename");
	curl_link_RPL = $(this).data("curl_link");
	logIndex = 0;
	getConsoleHTML(curl_link_RPL, process_RPL);
	$("#consoleResult").show();
	$(window).scrollTop($('#consoleBox').offset().top);
	if(isViewConsoleRunning == false){
		run_viewConsole();
	}
}

function run_viewConsole(){
	isViewConsoleRunning = true;
	if(logIndex == 0){
		$("#consoleBox").empty();
	}
	var pdlName = "processdefinition_" + process_RPL + ".xml";
	var run_url='viewConsole?profileName='+profileName_RPL+'&processFileName='+pdlName+'&logIndex='+logIndex;
	var mytime= "&ms="+new Date().getTime();
	run_url += mytime;
	$.ajax({
		type: "GET",
		url: curl_link_RPL + run_url,               
		cache:false,
		dataType : "json"
	})
	.done(function(data) {
		var theadArray = JSON.parse(data.header);
		logIndex = JSON.parse(data.logIndex); 
		var isEndOfLog = JSON.parse(data.isEndOfLog);
		var divContent="";
		for(var i=0;i<theadArray.length;i++){
			divContent = divContent + "<p>" + theadArray[i] + "</p>";
		}
		
		$("#consoleBox").prepend(divContent);
         	  
		if(isEndOfLog == true || $('#consoleResult').css('display') == 'none'){
			isViewConsoleRunning = false;
			return;
		} else {
			setTimeout(run_viewConsole, 1000);
		}
	});
}

