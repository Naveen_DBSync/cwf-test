function getEditableFieldContent(parentElement) {
	var editableFieldContent = '<div class="editable-field active toremove">'
		+ '<form id="labels-form" class="ajs-dirty-warning-exempt aui">'
			+ '<div class="inline-edit-fields" tabindex="1">'
				+ '<div class="field-group">'
					+ '<input class="long-field" id="targetEditableArea" name="summary" type="text" value=""/>'
					+ '</div>'
					+ '</div>'
					+ '<span class="overlay-icon throbber"></span>'
					+ '<div class="error-options toremove" tabindex="1">'
					+ '</div>'
					+ '<div class="save-options" tabindex="1">'
					+ '<button type="button" class="aui-button submit" accesskey="s" title="Press Alt+s to submit this form">'
					+ '<span class="aui-icon aui-icon-small aui-iconfont-success">&#x2714</span>'
					+ '</button>'
					+ '<button type="button" class="aui-button cancel" accesskey="`" title="Press Alt+` to cancel">'
					+ '<span class="aui-icon aui-icon-small aui-iconfont-close-dialog">&#x2716</span>'
					+ '</button>'
					+ '</div>'
					+ '</form>'
					+ '</div>';
	return editableFieldContent;
}

var previousValue = "";

$.fn.editableFieldFunction = function() {
	$(".editable-field").append($('<span class="overlay-icon aui-icon aui-icon-small aui-iconfont-edit"></span>')).on("click", function() {$(this).parent().click()});
	$(".editable-field").on("mouseover", function(){
		$(this).attr("title", "Click to edit");
	}).on("click", function() {
		if($(".editable-field.active.toremove")[0]) {
			$(".aui-button.submit").click();
			if($(".error-options.toremove").text().trim() != "") {
				return;
			}
		}
		$edittextbox = $(getEditableFieldContent($(this)));
		$edittextbox.css('min-width', '100%');
        $edittextbox.insertBefore($(this)).bind('keypress', function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
            	var validationParameter = {validatedValue : $("#targetEditableArea").val(), validationResult : false, errorMessage : ""};
            	$($(".editable-field.active.toremove")[0].nextSibling).trigger('validate', [validationParameter]);
            	if(validationParameter.validationResult) {
            		$($(".editable-field.active.toremove")[0].nextSibling).trigger('editsubmit', validationParameter.validatedValue);
            		$($(".editable-field.active.toremove")[0].nextSibling).text(validationParameter.validatedValue);
            		$($(".editable-field.active.toremove")[0].nextSibling).show()
                	$($(".editable-field.active.toremove")[0]).remove();
            		$(".editable-field").append($('<span class="overlay-icon aui-icon aui-icon-small aui-iconfont-edit"></span>')).on("click", function() {$(this).parent().click()});
            	} else {
            		$(".error-options.toremove").text(validationParameter.errorMessage);
            	}
            	e.preventDefault();
            }
        });
        previousValue = $(this).text();
        $("#targetEditableArea").val(previousValue);
        $(this).hide();
        $(".aui-button.cancel").on("click", function(event) {
        	$($(".editable-field.active.toremove")[0].nextSibling).show();
        	$($(".editable-field.active.toremove")[0].nextSibling).trigger('editsubmit', previousValue);
        	$($(".editable-field.active.toremove")[0].nextSibling).text(previousValue);
        	$($(".editable-field.active.toremove")[0]).remove();
        	event.preventDefault();
        	$(".editable-field").append($('<span class="overlay-icon aui-icon aui-icon-small aui-iconfont-edit"></span>')).on("click", function() {$(this).parent().click()});
        	previousValue = "";
        });
        $(".aui-button.submit").on("click", function(event) {
        	var validationParameter = {validatedValue : $("#targetEditableArea").val(), validationResult : false, errorMessage : ""};
        	$($(".editable-field.active.toremove")[0].nextSibling).trigger('validate', [validationParameter]);
        	if(validationParameter.validationResult) {
        		$($(".editable-field.active.toremove")[0].nextSibling).trigger('editsubmit', validationParameter.validatedValue);
        		$($(".editable-field.active.toremove")[0].nextSibling).text(validationParameter.validatedValue);
        		$($(".editable-field.active.toremove")[0].nextSibling).show()
            	$($(".editable-field.active.toremove")[0]).remove();
        		previousValue = "";
        	} else {
        		$(".error-options.toremove").text(validationParameter.errorMessage);
        	}
        	$(".editable-field").append($('<span class="overlay-icon aui-icon aui-icon-small aui-iconfont-edit"></span>')).on("click", function() {$(this).parent().click()});
        	event.preventDefault();
        });
        $("#targetEditableArea").focus();
        $("#targetEditableArea").on("blur", function() {
        	if($(event.relatedTarget).size() > 0 && $(event.relatedTarget)[0].className == "aui-button cancel") {
        		$(".aui-button.cancel").click();
        	} else {
	        	var validationParameter = {validatedValue : $("#targetEditableArea").val(), validationResult : false, errorMessage : ""};
	        	$($(".editable-field.active.toremove")[0].nextSibling).trigger('validate', [validationParameter]);
	        	if(validationParameter.validationResult) {
	        		$($(".editable-field.active.toremove")[0].nextSibling).trigger('editsubmit', validationParameter.validatedValue);
	        		$($(".editable-field.active.toremove")[0].nextSibling).text(validationParameter.validatedValue);
	        		$($(".editable-field.active.toremove")[0].nextSibling).show()
	            	$($(".editable-field.active.toremove")[0]).remove();
	        	} else {
	        		$(".error-options.toremove").text(validationParameter.errorMessage);
	        	}
	        	$(".editable-field").append($('<span class="overlay-icon aui-icon aui-icon-small aui-iconfont-edit"></span>')).on("click", function() {$(this).parent().click()});
        	}
    	});
	});
};

	
