$(function() {
     $( ".my-sec-nav-dropdown" ).each(function( index ) {
        var innerText = $(this).html();
		var codeToAdd = "<div class='sec-nav-dropdown-arrow'></div>";
		$(this).html(codeToAdd + "<div class='sec-nav-dropdown'>" + innerText + "</div>");
     });
	 
	 $( ".myArrowButton" ).each(function( index ) {
        var innerText = $(this).html();
		var codeToAdd = "<div class='highlight-button-arrow'></div>";
		$(this).html("<div class='arrow-button'>" + innerText + "</div>" + codeToAdd);
     });
	 
	 $( ".my-tree-dropdown" ).each(function( index ) {
        var innerText = $(this).html();
        var codeToAdd = "<div class='tree-dropdown-arrow'></div>";
		$(this).html(codeToAdd + "<div class='tree-dropdown'>" + innerText + "</div>");
		$(this).css("margin-right","3px");
		$(this).css("margin-bottom","3px");
    });
	
	$( ".my-info-hover-over" ).each(function( index ) {
		var innerText = $(this).html();
		var codeToAdd = "<div class='info-hover-over-arrow'></div>";
		$(this).html(codeToAdd + "<div class='info-hover-over'>" + innerText + "</div>");
	});
	
	$( ".my-popout-menu" ).each(function( index ) {
		var innerText = $(this).html();
		var crossMark = "<img class='properties_menu_x' src='images/properties_menu_x.png' onclick='close_info(this);' style='cursor: pointer;'>";
		var codeToAdd = "<div class='popout-arrow'></div>";
		$(this).html(codeToAdd + "<div class='popout-menu'>" + crossMark + innerText + "</div>");
	});
 });
	
function close_info(elemId){
	$(elemId).parent().parent().hide();
}