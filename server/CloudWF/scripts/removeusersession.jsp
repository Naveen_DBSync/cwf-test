<%@page import="com.avankia.appmashups.service.AuthenticationService"%>
<%@page import="com.avankia.appmashups.service.AuthenticatedUser"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	AuthenticatedUser user = AuthenticationService.getAUTHENTICATED_USER_MAP()
			.get(request.getParameter("username"));
	if (user != null) {
		user.sessionResetTime = new Date();
		user.isAuthenticated = false;
		AuthenticationService.getAuthenticationService()
				.deleteAuthenticatedUser(request.getParameter("username"));
	}
	session.invalidate();
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User session removed</title>
</head>
<body>
	<div>
		<h2>User session removed</h2>
		<br />
	</div>
</body>
</html>