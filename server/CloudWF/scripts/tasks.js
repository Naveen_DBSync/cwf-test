	var taskCounter = 0;
	var readerConditionCounter = 0;
	
	var xmlFormatter = {};
	
	var lastUsedQueryBuilder = '';
	if (stateJson.reader.property.hasOwnProperty("builder") && stateJson.reader.property.builder == 'simple' ) {
		lastUsedQueryBuilder = 'simple';
	} else {
		lastUsedQueryBuilder = 'advance';
	}
	
	var prevSrcAdp = '';
	var prevSrcTbl = '';
	var prevSrcCols = [];
	
	var possibleConditionOperations = {};
	
	var readableOperators = [];
	readableOperators.push('equals');
	readableOperators.push('not equal to');
	readableOperators.push('greater than');
	readableOperators.push('less than');
	readableOperators.push('greater or equal');
	readableOperators.push('less or equal');
	readableOperators.push('like');
	readableOperators.push('between');
	readableOperators.push('in');
	
	var actualOperators = [];
	actualOperators.push('=');
	actualOperators.push('!=');
	actualOperators.push('>');
	actualOperators.push('<');
	actualOperators.push('>=');
	actualOperators.push('<=');
	actualOperators.push('like');
	actualOperators.push('between');
	actualOperators.push('in');
		
	possibleConditionOperations["readableOperatorList"] = readableOperators;
	possibleConditionOperations["actualOperatorList"] = actualOperators;
	
	var defaultConditionRow = "<tr><td colspan=\"4\">No condition set. Please click on <a onclick=\"addNewCondition();\" style=\"text-decoration: underline;\">Add New Condition</a> to Add</td></tr>";
	
	// Creates new writer json
	function createNewWriterJson() {
		var writer = {
				"id": "", 
				"name": "", 
				"displayName": "", 
				"adapterType": "", 
				"property": {
					"table": "", 
					"tableExists": "", 
					"writetype": ""
				}
			};
		
		return writer;
	}
	
	// Creates new task json
	function createNewTaskJson() {
		var task = {
				"id": "",
				"name": "", 
				"map": createNewMapJson(), 
				"writer": createNewWriterJson()
		}; 
		
		return task;
	}
	
	// Creates new map json
	function createNewMapJson() {
		var map = {
				"id": "", 
				"name": "", 
				"displayName": "", 
				"description": "", 
				"sequence": 0, 
				"autocreate": false,
				"property": {
					"location": ""
				}
		}; 
		
		return map;
	}
	
	// Returns basic structure for status writer json
	function createNewStatusWriterJson() {
		var statusWriterJson = {
				"id": "", 
				"name": "", 
				"displayName": "", 
				"description": "", 
				"onSuccess": {
					"id": "", 
					"map": createNewMapJson()
				}, 
				"onFailure": {
					"id": "", 
					"map": createNewMapJson()
				}, 
				"property": {
					"table": "", 
					"writetype": ""
				}
		}; 
		
		return statusWriterJson;
	}
	
	// Capitalizes the first character of passed string
	function capitalizeFirstLetter(string) {
		return String(string).charAt(0).toUpperCase() + String(string).slice(1);
	}
	
	function loadReaderTablesDropdown() {
		var readerTableDropdown = createDropdown([], "Select Table", '', "reader-tables", false);
		document.write(readerTableDropdown);
		
		var newObjectList = [];
		for (var ctr=0; ctr<adapterTablesJson.length; ctr++) {
			if ($('#source-adapter option:selected').text().toLowerCase() == adapterTablesJson[ctr].name.toLowerCase()) {
				newObjectList = adapterTablesJson[ctr].objects;
			}
		}
		
		$('#reader-tables').empty();
        
		var newOption = $('<option>Select Table</option>');
		$('#reader-tables').append(newOption);
		
		for (var ctr=0; ctr<newObjectList.length; ctr++) {
			var newOption = $('<option>'+ newObjectList[ctr] +'</option>');
			$('#reader-tables').append(newOption);
		}
		
		$('#reader-tables').trigger("chosen:updated");
	}
	
	// collapse/expand status writer based upon status writer checkbox
	function handleStatusWriter(rowid) {
		if ($('#us-'+rowid).hasClass("fa-angle-down")) {
			$('#us-'+rowid).removeClass("fa-angle-down");
			$('#us-'+rowid).addClass("fa-angle-up");
			$('#us-'+rowid).closest('div').find('span').html(' Less');
		} else {
			$('#us-'+rowid).removeClass("fa-angle-up");
			$('#us-'+rowid).addClass("fa-angle-down");
			$('#us-'+rowid).closest('div').find('span').html(' More');
		}
		$('#exp-' + rowid).toggle();

	}
	
	function updateLink(element) {
		var thisId = $(element).attr('id');
		
		if (/^writerprop_autocreate-/.test(thisId)) {
			var ind = parseInt($(element).closest("tr").children('.task').children('.priority').html()) - 1;
			eleVal = $(element).val();
			stateJson.task[ind].writer.property.autocreate = eleVal;
			stateJson.task[ind].map.autocreate = eleVal;
			thisId = thisId.replace('writerprop_autocreate-', 'tc-');
			element = $("#" + thisId)[0];
		}
		
		if (/^tc-/.test(thisId)) {
			var newConnector = element.value;
			elemToChange = thisId.replace('tc-', 'op-');
			var selectedOption = $('#'+elemToChange).find(":selected").text();
			$('#'+elemToChange).empty();
			$('#'+elemToChange).append($('<option>Select Operation</option><option>Insert</option><option>Update</option><option>Upsert</option>'));
			for (var ctr=0; ctr<adapterTablesJson.length; ctr++) {
				if (newConnector.toLowerCase() == adapterTablesJson[ctr].name.toLowerCase()
						&& adapterTablesJson[ctr].type.toLowerCase() == "databaseadapter") {
					$('#'+elemToChange).append($('<option>Delete</option>'));
				}
			}
			$('#'+elemToChange).val(selectedOption).attr("selected");
			$('#'+elemToChange).trigger("chosen:updated");
		}
		
		updateObjectOptions(element);
		
		var sequence = parseInt($(element).closest("tr").children('.task').children('.priority').html());
		
		var newValue = element.value;
		
		var indexInTaskJsonArray = sequence - 1;
		
		if (/^tc-/.test(thisId)) {
			stateJson.task[indexInTaskJsonArray].writer.name = newValue;
			
			var adpIndex = -1;
			var selectedConnector = newValue;
			for (var ctr=0; ctr<adapterTablesJson.length; ctr++) {
				if (adapterTablesJson[ctr].name == selectedConnector) {
					adpIndex = ctr;
					break;
				}
			}
			
			if(stateJson.task[indexInTaskJsonArray].writer.property.autocreate == 'true'
					&& adapterTablesJson[adpIndex].objectsToCreate.indexOf($(element).closest("tr").find('select[id^=ao-]').val()) >= 0) {
				$(element).closest("tr").find("a#validmap").hide();
				$(element).closest("tr").find("span#automapping").show();
				stateJson.task[indexInTaskJsonArray].map.autocreate = true;
			} else {
				$(element).closest("tr").find("a#validmap").show();
				$(element).closest("tr").find("span#automapping").hide();
				stateJson.task[indexInTaskJsonArray].map.autocreate = false;
			}
		} else if (/^op-/.test(thisId)) {
			stateJson.task[indexInTaskJsonArray].writer.property.writetype = newValue;
		} else if (/^ao-/.test(thisId)) {
			stateJson.task[indexInTaskJsonArray].writer.property.table = newValue;
			
			var adpIndex = -1;
			var selectedConnector = $(element).closest("tr").find('select[id^=tc-]').val();
			for (var ctr=0; ctr<adapterTablesJson.length; ctr++) {
				if (adapterTablesJson[ctr].name == selectedConnector) {
					adpIndex = ctr;
					break;
				}
			}
			
			if(stateJson.task[indexInTaskJsonArray].writer.property.autocreate == 'true'
					&& adapterTablesJson[adpIndex].objectsToCreate.indexOf(newValue) >= 0) {
				$(element).closest("tr").find("a#validmap").hide();
				$(element).closest("tr").find("span#automapping").show();
				stateJson.task[indexInTaskJsonArray].map.autocreate = true;
			} else {
				$(element).closest("tr").find("a#validmap").show();
				$(element).closest("tr").find("span#automapping").hide();
				stateJson.task[indexInTaskJsonArray].map.autocreate = false;
			}
		} else if (/^su-tc-/.test(thisId)) {
			stateJson.task[indexInTaskJsonArray].statusWriter.name = newValue;
		} else if (/^su-op-/.test(thisId)) {
			stateJson.task[indexInTaskJsonArray].statusWriter.property.writetype = newValue;
		} else if (/^su-ao-/.test(thisId)) {
			stateJson.task[indexInTaskJsonArray].statusWriter.property.table = newValue;
		}
	}
	
	function removeLink(element) {
		$(element).removeClass("dropdownlink");
	}
	
	function makeItLink(element) {
		$(element).addClass("dropdownlink");
	}
	
	// This will update adapter objects list based upon target connector
	function updateObjectOptions(element) {
		var newConnector = element.value;
		
		var thisId = $(element).attr('id');
		
		var aoVal = '';
		var elemToChange = '';
		var roElemToChange = '';
		if (/^tc-/.test(thisId)) {
			elemToChange = thisId.replace('tc-', 'ao-');
			aoVal = $("#" + elemToChange).val().toLowerCase();
		} else if (/^su-tc-/.test(thisId)) {
			elemToChange = thisId.replace('su-tc-', 'su-ao-');
		} else if (/^source-adapter-simple/.test(thisId)) {
			elemToChange = 'source-adapter-tables-simple';
		}
		
		var newObjectList = [];
		
		for (var ctr=0; ctr<adapterTablesJson.length; ctr++) {
			if (newConnector.toLowerCase() == adapterTablesJson[ctr].name.toLowerCase()) {
				ind = ctr;
				$(adapterTablesJson[ctr].objects).each(function() {
					newObjectList.push(this.toString());
				});
				
				/* 
				 * we dont need any object other than writable objects in 
				 * rule objects for apiadapter
				 */
				if(adapterTablesJson[ctr].type.toLowerCase() == "apiadapter" 
					&& elemToChange.indexOf('ao-') >= 0) {
					newObjectList = [];
				}
				
				if (adapterTablesJson[ctr].hasOwnProperty("writableObjects") && elemToChange.indexOf('ao-') >= 0) {
					newObjectList = [];
					$(adapterTablesJson[ctr].writableObjects).each(function() {
						newObjectList.push(this.toString());
					});
				}
				
				newObjectList.sort();
				if(adapterTablesJson[ctr].type.toLowerCase() == "ftpadapter" && !elemToChange.startsWith('ao-')){
					newObjectList.unshift("&lt;Regular Expression Pattern&gt;");
				}
				
				if(elemToChange != 'source-adapter-tables-simple' && adapterTablesJson[ctr].type.toLowerCase() == "databaseadapter"){
					$("#source-adapter-tables-simple").children('option').each(function() {
						currentTable = $(this).val().replace(/\./ig, "_");
						if($(this)[0].selected == true && $(this).val() != "Select Target Object"
								&& newObjectList.indexOf(currentTable.toLowerCase()) == -1) {
							
							var ind = parseInt($(element).closest("tr").children('.task').children('.priority').html()) -1;
							
							if(ind) {
                                if (stateJson.task[ind].writer.property.tableExists == 'false'
                                    && stateJson.task[ind].writer.property.table == currentTable.toLowerCase()) {
                                    stateJson.task[ind].writer.property.autocreate = 'true';
                                    stateJson.task[ind].map.autocreate = true;
                                }

                                var ac = stateJson.task[ind].writer.property.autocreate;
                                var ele = $("#" + thisId.replace('tc-',
                                    'writerprop_autocreate-'));

                                if (ele[0] ? ac == "true" || ele[0].value == "true" : ac == "true") {
                                    if (adapterTablesJson[ctr].objectsToCreate.indexOf(
                                            currentTable.toLowerCase()) == -1) {
                                        adapterTablesJson[ctr].objectsToCreate.push(
                                            currentTable.toLowerCase());
                                    }
                                    newObjectList.unshift(
                                        currentTable.toLowerCase());
                                }
                            }
						}
					});
				}
			}
		}
		
		$('#'+elemToChange).empty();
        
		var newOption = elemToChange == 'source-adapter-tables-simple'
						? $('<option>Select Table</option>')
						: $('<option>Select Target Object</option>');
		
		$('#'+elemToChange).append(newOption);
		
		for (var ctr=0; ctr<newObjectList.length; ctr++) {
			var newOption = $('<option>'+ newObjectList[ctr] +'</option>');
			$('#'+elemToChange).append(newOption);
		}
		
		if(aoVal != '' && /^ao-/.test(elemToChange)) {
			
			var ind = parseInt($("#" + elemToChange).closest("tr").children('.task').children('.priority').html()) - 1;
			
			$("#" + elemToChange + " > option").each(function(j){
				if(this.text == aoVal) {
					$('#'+elemToChange).val(aoVal);
					return false;
				}
			});
		}
		
        $('#'+elemToChange).trigger("change");
	}
	
	/*
	 * Creates a new dropdown
	 * Returns - html for dropdown
	 * optionList eg - ["option1", "option2", "option3"]
	 * defaultOption eg - "Select Option"
	 * selectedOption - "option2"
	 */
	function createDropdown(optionList, defaultOption, selectedOption, newId, isHidden) {
		var newSelectBox = "<select id=\"" + newId + "\" class=\"form-control dropdownlink\" " + 
								" onclick=\"Event.stopImmediatePropagation;\" "  + 
								" onchange=\"updateLink(this);\" ";
		newSelectBox = newSelectBox + ">";
		
		newSelectBox = newSelectBox + "<option>" + defaultOption + "</option>";
		
		optionList.sort();
		
		var isSelectedOptionFound = false;
		for (var ctr=0; ctr<optionList.length; ctr++) {
			var option = optionList[ctr];
			newSelectBox = newSelectBox + "<option";
			if (option.toLowerCase() == selectedOption.toLowerCase()) {
				newSelectBox = newSelectBox + " selected ";
				isSelectedOptionFound = true;
			}
			newSelectBox = newSelectBox + ">" + option + "</option>";
		}
		
		if ((String(selectedOption).trim() != '') && (!isSelectedOptionFound) && (defaultOption != selectedOption)) {
			newSelectBox = newSelectBox + "<option selected>" + selectedOption + "</option>";
		}
		
		newSelectBox = newSelectBox + "</select>";
		return newSelectBox;
	}
	
	/*
	 * Creates a new dropdown
	 * Returns - html for dropdown
	 * optionList eg - ["option1", "option2", "option3"]
	 * defaultOption eg - "Select Option"
	 * selectedOption - "option2"
	 */
	function createDropdownWithGroups(optionList, defaultOption, selectedOption, newId, isHidden) {
		var newSelectBox = "<select id=\"" + newId + "\" class=\"form-control dropdownlink\" " + 
								" onclick=\"Event.stopImmediatePropagation;\" "  + 
								" onchange=\"updateLink(this);\" ";
		newSelectBox = newSelectBox + ">";
		
		newSelectBox = newSelectBox + "<option>" + defaultOption + "</option>";
		for (var ctr=0; ctr<optionList.length; ctr++) {
			var option = optionList[ctr];
			newSelectBox = newSelectBox + "<option";
			if (option.toLowerCase() == selectedOption.toLowerCase()) {
				newSelectBox = newSelectBox + " selected ";
			}
			newSelectBox = newSelectBox + ">" + option + "</option>";
		}
		newSelectBox = newSelectBox + "</select>";
		return newSelectBox;
	}
	
	/*
	 * This method creates a w2ui consumable task json 
	 * Loads new task json with data if provided in parametes
	 */
	function createTask(currentTask) {
		taskCounter++;
		
		var targetConnector = 'Select Target Connector';
		var operation = 'Select Operation';
		var table = 'Select Target Object';
		
		var mapId = '';
		
		var newTask = {};
		newTask["recid"] = -1;
		
		if (currentTask != null) {
			targetConnector = currentTask.writer.name;
			if (targetConnector.trim().toLowerCase() == "consoleadapter") {
				operation = "Insert";
				table = "Console";
				
				currentTask.writer.property.writetype = operation;
				currentTask.writer.property.table = table;
			} else {
				operation = currentTask.writer.property.writetype;
				
				// Java adapter's dont have writetype, if we upload any old Java adapter based pdl
				// their writetype will be undefined
				// So, defaulting writetype to Upsert
				if(typeof operation === "undefined") {
					operation = "Upsert";
				}
				
				table = currentTask.writer.property.table;
			}
			
			mapId = currentTask.map.id;
		}
		operation = capitalizeFirstLetter(operation);
		
		var statusWriterConnector = 'Select Update Target';
		var statusWriterOperation = 'Select Operation';
		var statusWriterTable = 'Select Target Object';
		
		var statusWriter = '';
		if ((currentTask != null) && (currentTask.statusWriter != null)) {
			newTask["status"] = true;
			statusWriter = "checked";
			statusWriterConnector = currentTask.statusWriter.name;
			if(statusWriterConnector.trim().toLowerCase() == "consoleadapter"){
				statusWriterOperation = 'Select Operation';
				statusWriterTable = 'Select Target Object';
			}
			else{
			statusWriterOperation = currentTask.statusWriter.property.writetype;
			statusWriterTable = currentTask.statusWriter.property.table;
			}
			
		}
		statusWriterOperation = capitalizeFirstLetter(statusWriterOperation);
		
		// Create writer select box using available adapters
		var writerSelectBox = createDropdown(availableAdaptersJson, "Select Target Connector", targetConnector, "tc-"+taskCounter, true);
		
		// Create operation select box using supported operations
		var possibleOperations = ["Insert","Update","Upsert"];
		var operationSelectBox = createDropdown(possibleOperations, "Select Operation", operation, "op-"+taskCounter, true);
		
		// Load tables based upon target connector
		var selectedAdapterTables = [];
		for (var ctr=0; ctr<adapterTablesJson.length; ctr++) {
			if (targetConnector.toLowerCase() == adapterTablesJson[ctr].name.toLowerCase()) {
				
				$(adapterTablesJson[ctr].objects).each(function() {
					selectedAdapterTables.push(this.toString());
				});
				
				if (adapterTablesJson[ctr].hasOwnProperty("writableObjects")) {
					$(adapterTablesJson[ctr].writableObjects).each(function() {
						selectedAdapterTables.push(this.toString());
					});
				}
				
				break;
			}
		}
		
		// Create target table select box
		var adapterTablesSelectBox = createDropdown(selectedAdapterTables, "Select Target Object", table, "ao-"+taskCounter, true);
		
		// Create status writer select box using available adapters
		var statusUpdateSelectBox = createDropdown(availableAdaptersJson, "Select Update Target", statusWriterConnector, "su-tc-"+taskCounter, true);
		
		// Create operation select box using supported operations for status update
		var statusUpdateOperationSelectBox = createDropdown(possibleOperations, "Select Operation", statusWriterOperation, "su-op-"+taskCounter, true);
		
		// Load tables based upon reader connector for status update
		var sourceAdapterTables = [];
		for (var ctr=0; ctr<adapterTablesJson.length; ctr++) {
			if (statusWriterConnector.toLowerCase() == adapterTablesJson[ctr].name.toLowerCase()) {
				sourceAdapterTables = adapterTablesJson[ctr].objects;
			}
		}
		
		// Create source update table select box
		var sourceTablesSelectBox = createDropdown(sourceAdapterTables, "Select Target Object", statusWriterTable, "su-ao-"+taskCounter, true);
		
		var ruleName = currentTask != null ? currentTask.name : "NewRule" + taskCounter;
		
		newTask["name"] = ruleName;
		var isAutoMapped = currentTask != null && currentTask.writer != null && currentTask.writer.property != null && currentTask.writer.property.autocreate == "true";
		
		
		var readerTableEqualWriter = false;
		
		if(currentTask != null) {
			
			var readerTable = stateJson.reader.property.table;
			if(typeof readerTable !== "undefined") {
				readerTable = readerTable.toLowerCase();
			}
			
			readerTableEqualWriter = currentTask.writer.property.table.toLowerCase() == readerTable;
		}
		// create html for w2ui to render
		var stepHTML = "<div class=\"stepDiv row col-lg-12\">" +
		
							"<div class=\"col-lg-3\" >" + 
								writerSelectBox + 
							"</div>" +
							
							"<div class=\"col-lg-3\" >" + 
								operationSelectBox + 
							"</div>" + 	
							
							"<div class=\"col-lg-3\" >" + 	
								adapterTablesSelectBox + 
							"</div>" +
							
							"<div class=\"col-lg-2\" style=\"padding-top: 10px;\">" + 
							"Using <span id=\"automapping\" style=\"display: " + ((isAutoMapped == true && readerTableEqualWriter == true) ? "block" : "none") + ";\">Auto Map</span><a id=\"validmap\" data-target=\"#mapWindow\" data-maptype=\"writermap\" style=\"text-decoration: underline; cursor: pointer; display: " + (((isAutoMapped == true && readerTableEqualWriter == false) || (isAutoMapped == false)) ? "block" : "none") + ";\">Map</a>" +
							"</div>" + 
							
							"<div class=\"col-lg-1\" style=\"padding-left: 0px; margin-top: 7px; \" onclick=\"handleStatusWriter('" + taskCounter + "'); Event.stopImmediatePropagation;\">" + 
								"<i id=\"us-"+taskCounter+"\" style=\"cursor: pointer; font-size: 1.7em;\" class=\"fa fa-angle-down\"></i>" + 
								"<span> More</span>" + 
							"</div>" + 
							
						"</div>" + 
						
						"<div class=\"expandDiv row col-lg-12\" id=\"" + "exp-"+taskCounter + "\" style=\"margin-top: 10px; display: none; \">" +
							
							"<div class=\"tabs-container\" >" + 
                        		"<ul class=\"nav nav-tabs\" id=\"extraTab\">" +
                            		"<li class=\"active\" >" + 
                            			"<a data-toggle=\"tab\" data-target=\"#statuswriter-" + taskCounter + "\">" + "Update Source " + 
                            				"<label class=\"checkbox-inline i-checks\" style=\"float: left; margin-right: 5px;\">" + 
                            					"<input type=\"checkbox\" id=\"sourcecheck-" + taskCounter + "\" >" + 
                            				"</label>" + 
                            			"</a>" +  
                            		"</li>" + 
                            		"<li>" + 
                            			"<a data-toggle=\"tab\" data-target=\"#properties-" + taskCounter + "\">Properties</a>" + 
                            		"</li>" + 
								"</ul>" + 
								"<div class=\"tab-content\" style=\"background-color: #FFFFFF;\">" + 
									"<div id=\"statuswriter-" + taskCounter + "\" class=\"tab-pane active\" >" + 
						
										"<div class=\"panel-body\">" + 
										
											"<div id=\"disabledStatusMessage-" + taskCounter + "\">Please select the checkbox above to enable source update</div>" + 
										
											"<div class=\"col col-lg-3\" style=\"padding-bottom: 2px; \">" + 
												statusUpdateSelectBox + 
											"</div>" +
										
											"<div class=\"col col-lg-2\" style=\"padding-bottom: 2px; padding-left: 0px; padding-right: 0px;\">" + 
												statusUpdateOperationSelectBox + 
											"</div>" +
											
											"<div class=\"col col-lg-3\" style=\"padding-bottom: 2px; \">" + 
												sourceTablesSelectBox + 
											"</div>" +
											
											"<div class=\"col col-lg-4\" style=\"margin-top: 10px; text-align: right\">" + 
												"On Success " + 
												"<a data-target=\"#mapWindow\" data-maptype=\"writermap-success\" style=\"text-decoration: underline; cursor: pointer;\">Map</a>" + 
												" On Error " + 
												"<a data-target=\"#mapWindow\" data-maptype=\"writermap-failure\" style=\"text-decoration: underline; cursor: pointer;\">Map</a>" +
											"</div>" + 
										"</div>" + 
									"</div>" + 	
									"<div id=\"properties-" + taskCounter + "\" class=\"tab-pane\" >" + 
										"<div class=\"panel-body\">" + 
											"<div class='row'>" + 
												"<div class='target-props-header col col-lg-6' style='border-right: solid 1px black;'>" +
													"<h3>Target Properties</h3>" + 
													"<div class='target-props'>" +
														"<div id='no_writer_msg-" + taskCounter + "' >" + 
															"<span style='color: red'>Please select target data source.</span>" + 
														"</div>" +
													"</div>" + 
												"</div>" + 
												"<div class='updatesource-props-header col col-lg-6' >" +
													"<h3>Update Source Properties</h3>" + 
													"<div class='updatesource-props'>" +
														"<div id='no_sw_msg-" + taskCounter + "' >" + 
															"<span style='color: red'>Please check update source.</span>" + 
														"</div>" +
													"</div>" + 
												"</div>" + 
											"</div>" + 
										"</div>" + 
									"</div>" +  
								"</div>"
							"</div>" + 
						"</div>" ;
						
		newTask["step"] = stepHTML;
		
		return newTask;
	}
	
	function loadOperator(){
		$.ajax({
			 async : false,
			 type : "GET",
			 url : curl_link + "getOperator", 
			 data : { adapterName : $("#source-adapter-simple").val()},
		 }).done(function(data) {
			 possibleConditionOperations.readableOperatorList = data.operators;
		 });
	}
	
	// Load tasks if state if available 
	var taskJsonArray = [];
	for (var taskCtr in stateJson.task) {
		var currentTask = stateJson.task[taskCtr];
		var t = createTask(currentTask);
		t.recid = parseInt(taskCtr, 10) + 1;
		taskJsonArray.push(t);
	}
	
	changeQuery = false;
	
	function updateQuery() {
		
		if (!changeQuery || $('#source-adapter-tables-simple').val().toLowerCase() == "select target object") {
			return;
		}
		
		var result = buildQueryForSimple();
		if(result.sql != undefined){
			$('#source-adapter-advance').val($('#source-adapter-simple').val());
			$("#readOnlyQuery").text(result.sql);
		}
	}

	jQuery(document).ready(function(){
		
		for (var ctr=0; ctr<stateJson.task.length; ctr++) {
			stateJson.task[ctr].map.sequence = ctr+1;
		}
		
		// On change of Status Writer, If Console Adapter --> disable, If other adapters --> enable, the operation and table name 
		$('div #new_table').on('change', 'select[id^="su-tc-"]', function() {
			 consAdapEnableAndDisable(this);
		});
		
		loadOperator();
	
		$('#conditionTableBody').on('click', '.variables', function() {
			$(this).closest('td').find('input[id^=value-]').val($(this).find('a').text());
		});
		
		loadTriggerProperties(true);
		
		var tableBody = $('#newTaskTableBody');
		var data = taskJsonArray;
		$.each(data, function (i, item) {
			addRow(tableBody, item);
		});
		$('.footable').trigger('footable_initialize');
		$('div #new_table select[id^="su-tc-"]').each(function(){
			 consAdapEnableAndDisable(this);
		});
		
		 $('#new_table').on('click','div #extraTab a', function() {
	            $(this).tab('show');
	     });
		 
		 $('#triggerTab a').on('click', function() {
			 
			 if ($(this).data('target') == '#simpleQueryBuildder') {
				 lastUsedQueryBuilder = 'simple';
				 prevSrcAdp = $("#" + "source-adapter-simple" + " :selected").text();
			 } else if ($(this).data('target') == '#advancedQueryBuilder') {
				 lastUsedQueryBuilder = 'advance';
				 prevSrcAdp = $("#" + "source-adapter-advance" + " :selected").text();
			 }
			 
	         $(this).tab('show');
	     });
		 
		 if (stateJson.reader.property.hasOwnProperty("builder") &&
				 stateJson.reader.property.builder == 'simple' ) {
			 loadSimpleTrigger();
			 $('.nav-tabs a[data-target="#simpleQueryBuildder"]').tab('show');
		 } else {
			 
			 $("#conditionTableBody").append(defaultConditionRow);
			 $('#condition_table span.footable-toggle').hide();
			 $('.nav-tabs a[data-target="#advancedQueryBuilder"]').tab('show');
		 }
		 
		 var rowCtr = -1;
			$.each(data, function (i, item) {
				rowCtr++;
				for (var i=0; i<adapterTablesJson.length; i++) {
					 if (stateJson.task[rowCtr].writer.name.toLowerCase() == adapterTablesJson[i].name.toLowerCase() 
							 && adapterTablesJson[i].type.toLowerCase() == "databaseadapter") {
						 
						var s = 'select[id^=tc-'+ (1 + rowCtr) +']';
						
						var s1Id = "ao-" + (1 + rowCtr);
						$("#" + s1Id + " > option").each(function(j){
							if (this.text == stateJson.task[rowCtr].writer.property.table) {
						    	$("#" + s1Id).val(stateJson.task[rowCtr].writer.property.table);
						        return false;
						    }
						});
						
						$(s).trigger("change");
						
					}
				}
			});
		 
		 prevSrcAdp = stateJson.reader.name;
		 
		 
		 $("#simpleQueryResultTable").hide();
		 
		 $('#result-anchor').on('click', function() {
			if ($('#arrow-icon').hasClass("fa-angle-down")) {
				$('#arrow-icon').removeClass("fa-angle-down");
				$('#arrow-icon').addClass("fa-angle-up");
			} else {
				$('#arrow-icon').removeClass("fa-angle-up");
				$('#arrow-icon').addClass("fa-angle-down");
			}
	     });
		 
		
		 $(document).ready(function(){
			 
			if (seqNo != 'null' && mapId != 'null') {
					$('#validmap').trigger("click");
				}				
			 
			 // calling the updateNewTableChanges function since select tag in new_table has changed
			 $('#new_table').on('change','select', updateNewTableChanges);
			 
			 // Update reader datasource in statejson
			 $('#simpleQueryBuilder').on('change','select', updateReaderInStateJson);
			 $('#advancedQueryBuilder').on('change','select', updateReaderInStateJson);
			 
		 });
		 
		 function updateReaderInStateJson() {
			 var selectedId = $(this).attr('id');
			 if (selectedId == 'source-adapter-advance' || selectedId == 'source-adapter-simple') {
				 var selectedVal = $("#" + selectedId + " :selected").text();
				 stateJson.reader.name = selectedVal;
				 
				 lastUsedQueryBuilder = selectedId.split('-')[2];
				 
				 prevSrcAdp = stateJson.reader.name;
				 
				 loadTriggerProperties(false);
			 }
		 }
		 
		 
		 // Updating the json object when any dropdown is changed from ui
		 function updateNewTableChanges(){
		  var index = $(this).closest('tr').index()
		  var task = stateJson.task[index];
		  var selectedId = $(this).attr('id');
		  var selectedVal = $("#" + selectedId + " :selected").text();
		  
		  if (selectedVal == 'Select Target Connector' 
			  || selectedVal == 'Select Operation' 
				  || selectedVal == 'Select Target Object' 
					  || selectedVal == 'Select Update Target'
						  || selectedVal == 'Select Field') {
			  selectedVal = '';
		  }
		  
		  if (selectedId.indexOf('tc-') == 0){
			  //writer name
			  task.writer.name = selectedVal;
			  loadTargetProps(selectedId);
			  
		  } else if(selectedId.indexOf('op-') == 0){
			  //operation name
			  task.writer.property.writetype = selectedVal.toLowerCase();
		  } else if(selectedId.indexOf('ao-') == 0){
			  //table name
			  task.writer.property.table = selectedVal;
		  } else if((task.hasOwnProperty("statusWriter")) && (selectedId.indexOf('su-tc-') == 0)){
			  //status writer name
			  task.statusWriter.name = selectedVal;
			  loadUpdateSourceProps(selectedId);
			  
		  } else if((task.hasOwnProperty("statusWriter")) && (selectedId.indexOf('su-op-') == 0)){
			  //status writer operation name
			  task.statusWriter.property.writetype = selectedVal.toLowerCase();
		  } else if((task.hasOwnProperty("statusWriter")) && (selectedId.indexOf('su-ao-') == 0)){
			  //status writer table name
			  task.statusWriter.property.table = selectedVal;
		  }
		 }
		 
		$('body').on('ifChanged', 'input', function(event) {
			
			 var id = $(this).attr('id').split('-')[1];
			 
			 var isChecked = false;
			 if ($(this).parent().hasClass('checked')) {
				 isChecked = true;
			 }
			 
			 isChecked = !isChecked;
			 var recId = parseInt($(this).closest("tr").children(".task").children(".priority").text());
			 
			 if (isChecked) {
				 $('#disabledStatusMessage-'+id).hide();
				 $('#disabledStatusMessage-'+id).siblings().show();
				 $('#statuswriter-'+id+' *').removeAttr('disabled');
				 stateJson.task[recId-1]["statusWriter"] = createNewStatusWriterJson();

				 stateJson.task[recId-1].statusWriter.name = $("#su-tc-"+id + " :selected").text();
				 stateJson.task[recId-1].statusWriter.property.writetype = $("#su-op-"+id + " :selected").text();
				 stateJson.task[recId-1].statusWriter.property.table = $("#su-ao-"+id + " :selected").text();
				 
				 loadUpdateSourceProps('su-tc-'+id);
				 var updateTargetObj = $(this).closest('.tabs-container').find('select[id^="su-tc-"]');
				 consAdapEnableAndDisable(updateTargetObj);
				 
			 } else {
				 $('#disabledStatusMessage-'+id).show();
				 $('#disabledStatusMessage-'+id).siblings().hide();
				 $('#statuswriter-'+id+' *').attr('disabled', true);
	        		
				 delete stateJson.task[recId-1]["statusWriter"];
				 
				 $('#updatesource_props-'+id).html('');
				 $('#no_sw_msg-'+id).show();
				 
			 }
			 
		 });
		 
		$('#source-adapter-advance').on('change', function() {
			
			// Update source adp for advance builder as well.
            if ($("#source-adapter-simple").val() != $(this).val()) {
            	$("#source-adapter-simple").val($(this).val());
				$('#source-adapter-simple').trigger("change");
            }
            
		});
		
		 $('#source-adapter-simple').on('change', function() {
			 changeQuery = true; 
			var newSrc = $(this).val();
			 	loadOperator();
	            
	            for (var ctr=0; ctr<adapterTablesJson.length; ctr++) {
	            	if (adapterTablesJson[ctr].name == newSrc) {
	            		
	            		var adpType = adapterTablesJson[ctr].type;
	            		if (adpType == 'QuickBooksAdapter2') {
	            			$("#executeButton").prop("disabled",true);
	            		} else {
	            			$("#executeButton").prop("disabled",false);
	            		}
	            		break;
	            	}
	            }
	            
	            $('#conditionTableBody').empty();
	            $("#conditionTableBody").append(defaultConditionRow);
	            readerConditionCounter = 0;
	            $('#condition_table span.footable-toggle').hide();
	            
	            
	            // Update source adp for advance builder as well.
	            if ($("#source-adapter-advance").val() != newSrc) {
	            	$("#source-adapter-advance").val(newSrc);
					$('#source-adapter-advance').trigger("change");
	            }
				
				
	     });
		 
		 $('#source-adapter-tables-simple').on('change', function() {
			 stateJson.reader.property["table"] = $("#source-adapter-tables-simple").val();
			 $('#conditionTableBody').empty();
			 $("#conditionTableBody").append(defaultConditionRow);
			 readerConditionCounter = 0;
			 $('#condition_table span.footable-toggle').hide();
			 
			 $("#filter-logic").val('');
			 
			 // Reset Automaps
			 var ctr = -1;
			 $('#newTaskTableBody tr').each(function(){
				 ctr++;
				 
				 for (var i=0; i<adapterTablesJson.length; i++) {
					 if (stateJson.task[ctr].writer.name.toLowerCase() == adapterTablesJson[i].name.toLowerCase() 
						 && adapterTablesJson[i].type.toLowerCase() == "databaseadapter") {
			
						var s1Id = "ao-" + (1 + ctr);
						$("#" + s1Id + " > option").each(function(j){
							if (this.text == stateJson.task[ctr].writer.property.table) {
						    	$("#" + s1Id).val(stateJson.task[ctr].writer.property.table);
						        return false;
						    }
						});

						$(this).find('select[id^=tc-]').trigger("change");
					 }
				 }
			 });
			 generateQuery();
			 xmlFormatter.isQueryChanged = true;
		 });
		 
		 $( "#source-adapter-tables-simple" ).change(function() {
			 adpType = getSelectedAdapterType();
			 var curSrcTbl = $('#source-adapter-tables-simple').val();
			 if ((adpType == 'JavaAdapter' || adpType == 'APIAdapter') && (curSrcTbl != 'Select Table')) {
				 addNewConditionForJavaAdptet();
			 }
			});
		 
		 //write readonly Query for adv query builder
		 $("#readOnlyQuery").text(stateJson.reader.property.query);
		 
		 $('#trigger_props').on('change', 'input[id^="readerprop-"]', function(event) {
			 
			 var thisId = $(this).attr('id');
			 var propName = thisId.split('-')[1];
			 
			 var value = $(this).val().trim();
			 
			 stateJson.reader.property[propName] = value;
			 
		 });
		 
		 $('#new_table').on('change', '[id^="writerprop_"]', function() {
			 var thisId = $(this).attr('id');
			 
			 var propName = thisId.split('-')[0].split('_')[1];
			 
			 var value = $(this).val().trim();
			 
			 var sequence = $(this).closest("tr").find(".priority").html();
			 sequence = parseInt(sequence);
			 
			 stateJson.task[sequence-1].writer.property[propName] = value;
		 });
		 
		 $('#new_table').on('change', 'input[id^="updatesourceprop_"]', function() {
			 var thisId = $(this).attr('id');
			 
			 var propName = thisId.split('-')[0].split('_')[1];
			 
			 var value = $(this).val().trim();
			 
			 var sequence = $(this).closest("tr").find(".priority").html();
			 sequence = parseInt(sequence);
			 
			 stateJson.task[sequence-1].statusWriter.property[propName] = value;
		 });
	 });

	
	function consAdapEnableAndDisable(updateTargetObj){
		if($(updateTargetObj).val() == 'ConsoleAdapter'){
			$(updateTargetObj).parent().siblings().find('select[id^="su-op-"]').attr('disabled', true);
			$(updateTargetObj).parent().siblings().find('select[id^="su-ao-"]').attr('disabled', true);
			$(updateTargetObj).parent().siblings().find('select[id^="su-op-"]').attr('style', 'color: #dddddd');
			$(updateTargetObj).parent().siblings().find('select[id^="su-ao-"]').attr('style', 'color: #dddddd');
			$(updateTargetObj).parent().siblings().find('select[id^="su-op-"]').attr('title', 'Not Applicable');
			$(updateTargetObj).parent().siblings().find('select[id^="su-ao-"]').attr('title', 'Not Applicable');
		 } else {
			$(updateTargetObj).parent().siblings().find('select[id^="su-op-"]').removeAttr('disabled');
			$(updateTargetObj).parent().siblings().find('select[id^="su-ao-"]').removeAttr('disabled');
			$(updateTargetObj).parent().siblings().find('select[id^="su-op-"]').removeAttr('style');
			$(updateTargetObj).parent().siblings().find('select[id^="su-ao-"]').removeAttr('style');
			$(updateTargetObj).parent().siblings().find('select[id^="su-op-"]').removeAttr('title');
			$(updateTargetObj).parent().siblings().find('select[id^="su-ao-"]').removeAttr('title');
		 }
	}
	
	function loadTargetProps(elemId) {
		 var tn = $('#'+elemId).closest("tr").find(".priority").html();
		 tn = parseInt(tn); 
		 var resultDiv = $('#'+elemId).closest("tr").find('div[id^=properties-]').find(".target-props");
		
		 var rowId = $('#'+elemId).closest("tr").find('div[id^=properties-]').attr('id').split('-')[1];
		 
		 
		 $('#no_writer_msg-'+rowId).hide();
			
		$('#task_props-'+rowId).html('');
			
			if (stateJson.task[tn-1].writer.name == 'Select Target Connector' || stateJson.task[tn-1].writer.name.trim() == '') {
				$('#no_writer_msg-'+rowId).show();
				return;
			}
		 
		 $.ajax({
			 type : "POST",
			 url : curl_link + "getTaskProperties", 
			 data : { taskJson : JSON.stringify(stateJson.task[tn-1]), rowId : rowId},
		 }).done (
			 function(data) {
				 resultDiv.html(data);
			 }
		);
	 
	 }
	
	function loadUpdateSourceProps(elemId) {
		var tn = $('#'+elemId).closest("tr").find(".priority").html();
		tn = parseInt(tn); 
		var resultDiv = $('#'+elemId).closest("tr").find('div[id^=properties-]').find(".updatesource-props");
		
		var rowId = $('#'+elemId).closest("tr").find('div[id^=properties-]').attr('id').split('-')[1];
		
		
		$('#no_sw_msg-'+rowId).hide();
		
		$('#updatesource_props-'+rowId).html('');
		
		if (stateJson.task[tn-1].statusWriter.name == 'Select Update Target' || stateJson.task[tn-1].statusWriter.name.trim() == '') {
			$('#no_sw_msg-'+rowId).show();
			return;
		}
		
		$.ajax({
			type : "POST",
			url : curl_link + "getStatusWriterProperties", 
			data : { taskJson : JSON.stringify(stateJson.task[tn-1]), rowId : rowId},
		}).done (
				function(data) {
					resultDiv.html(data);
				}
		);
		
	}
	
	
	function loadICheck(element) {
		$(element).iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });
	}
	
	//function to load existing simple trigger
	function loadSimpleTrigger() {
		if (stateJson.reader.property.table.trim() != '') {
			$("#source-adapter-tables-simple").val(stateJson.reader.property.table.trim());
		}
		
		$('#conditionTableBody').empty();
		$("#conditionTableBody").append(defaultConditionRow);
		$('#condition_table span.footable-toggle').hide();
		
		var conditionsJsonArray = JSON.parse(stateJson.reader.property.conditions);
		
		if (conditionsJsonArray.length != 0) {
			
			for (var ctr=0; ctr<conditionsJsonArray.length; ctr++) {
				
				var currentCondition = conditionsJsonArray[ctr];
				
				if (Object.keys(currentCondition).length == 0) {
					continue;
				}
				
				addNewCondition();
				
				if (currentCondition.field.trim() != '') {
					$("#field-"+(ctr+1)).val(currentCondition.field);
				}
				
				if (currentCondition.operator.trim() != '') {
					$("#operation-"+(ctr+1)).val(currentCondition.operator);
				}
				
				if (currentCondition.value.trim() != '') {
					$("#value-"+(ctr+1)).val(currentCondition.value);
				}
				
			}
		}
		
		$("#filter-logic").val(stateJson.reader.property.filter_logic.trim());
	}

	//function to save complete state
	function extendedButton_01() {
		saveCompleteState();
	}

	function saveCompleteState() {
		
		var triggerValidations = generateQuery();
		triggerValidations.reader;
		var errorHTML = '';
			
		// validation for xmlformatter. checks the query used to build xmlformatter and 
		// reader query is equals or not.
		if($("div .tabs-container #configure").length > 0 && $("#readerprop-xmlformatter").text().trim().length > 0){
			if(xmlFormatter.isQueryChanged != undefined) {
				if(!isValidXmlFormatterValue()){
					triggerValidations.errors.push("Invalid XMLFormatter value");
				}
			}
		}
	
		var regex = $("#readerprop-regularexp").val();
		if($("#source-adapter-tables-simple").val() == "<Regular Expression Pattern>" && getSelectedAdapterType().toLowerCase() == "ftpadapter" &&
        		( regex == null || regex == "")) {
        		swal("Can't execute query","Please provide Regular Expression in Properties tab!", "error");
        		return;
        	}
		
		if(availableAdaptersJson.length <= 1){
			triggerValidations.errors.push("Connecotrs are not configured.");
		}
		
		if($.inArray(stateJson.reader.name, availableAdaptersJson) == -1) {
			triggerValidations.errors.push("Selected Reader not available");
		}
		
		for (var ctr =0 ; ctr < stateJson.task.length; ctr++) {
			if($.inArray(stateJson.task[ctr].writer.name, availableAdaptersJson) == -1) {
				triggerValidations.errors.push("Selected Reader writer not available");
				break;
			}
			
		}
		
		
		if (triggerValidations.errors.length != 0) {
			for (var ctr=0; ctr<triggerValidations.errors.length; ctr++) {
				errorHTML += triggerValidations.errors[ctr] + "<br/>";
			}
		} 
		
		var flowValidationArray = validateTask();
		if(flowValidationArray.length != 0) {
			for (var ctr=0; ctr<flowValidationArray.length; ctr++) {
				errorHTML += flowValidationArray[ctr] ;
			}
		}
		
		if(errorHTML.trim() != '') {
			errorHTML = "<div style='overflow-y: auto; max-height: 100px; text-align: left; margin-left: 17%;'>" + errorHTML + "</div>";
			swal({
			     title: "Workflow not saved.",
			     text: errorHTML,
			     html: true,
			     type: "error"
			});
			
		} else {
			saveTrigger(triggerValidations.reader);
		}
	}
	
	function generateQuery(){
		var triggerValidations = {};
		
		var builder = '';
			
		if ($("#simpleQueryBuilder").hasClass("active")) {
			builder = 'simple';
		} else if ($("#advancedQueryBuilder").hasClass("active")) {
			builder = 'advance';
		} else {
			builder = lastUsedQueryBuilder;
		}
		
		if (builder == 'simple') {
			triggerValidations = validateSimpleTrigger();
			if(triggerValidations.errors.length == 0){
				var result = buildQueryForSimple();
				triggerValidations.reader.property.query = result.sql;
			}
		} else if (builder == 'advance')  {
			triggerValidations = validateAdvancedTrigger();
		}
		
		return triggerValidations;
	}
	
	function isValidXmlFormatterValue(){
		if(xmlFormatter.query == undefined && xmlFormatter.isQueryChanged == undefined){
			return true;
		} else if(xmlFormatter.query == undefined){
			return false;
		}
		var xmlFormatterQuery = xmlFormatter.query.replace(/\s\s+/g, ' ');
		xmlFormatterQuery = xmlFormatterQuery.trim();
		var readerQuery = stateJson.reader.property.query.replace(/\s\s+/g, ' ');
		readerQuery = readerQuery.trim();
		return xmlFormatterQuery == readerQuery;
	}
	
	//function to validate advanced trigger
	function validateAdvancedTrigger() {
		var validations = { "errors" : [], 
				"reader" : {}
			  };
		
		if ($("#source-adapter-advance").val() == 'Select Datasource' ) {
			validations.errors.push("Datasource missing.");
			return validations;
		}
		
		if (stateJson.reader.property.hasOwnProperty("batchsize")) {
			var bsize = stateJson.reader.property.batchsize;
			if (isNaN(bsize)) {
				validations.errors.push("Trigger Batch Size should be a positive number.");
				return validations;
			} else {
				var bsizeNum = parseInt(bsize);
				if (bsizeNum < 1) {
					validations.errors.push("Trigger Batch Size should be a positive number.");
					return validations;
				}
			}
		}
		
		var query = $("#readOnlyQuery").text();
		
		var advancedReaderJson = stateJson.reader;
		advancedReaderJson.name = $("#source-adapter-advance").val();
		advancedReaderJson.property.query = query;
		advancedReaderJson.property["builder"] = "advance";
		
		advancedReaderJson.property["table"] = "";
		advancedReaderJson.property["conditions"] = JSON.stringify([]);
		advancedReaderJson.property["filter_logic"] = "";
			
		validations.reader = advancedReaderJson;
		
		return validations; 
	}
	
	//function to save simple trigger
  function saveTrigger(triggerContent) {
    var jsonString = JSON.stringify(triggerContent);
    var taskArray = JSON.stringify(stateJson.task);

    $.ajax({
      method: "POST",
      url: curl_link + "saveSimpleTrigger",
      data: {reader: jsonString, state: stateNameGlobal, tasks: taskArray},
      dataType: "json"
    })
    .done(
        function (data) {
          var saveResult = data.saveResult;

          if (saveResult === "success") {
            var $source = $("#source-adapter-advance");
            $source.val(triggerContent.name);
            $source.trigger("change");

            $(".readOnlyQuery").text(triggerContent.property.query);
            swal("Saved!", saveResult, "success");
          } else if (saveResult === "invalidquery") {
            swal("Trigger Not Saved",
                "Query is not valid. Please check your trigger again", "error");
          } else {
            swal("Trigger Not Saved",
                "Some technical issue. Please try it again later.", "error");
          }
        }
    );
  }
	
	//function to validate simple trigger (will be validating adv trigger too later on)
	function validateSimpleTrigger() {
		var validations = { "errors" : [], 
							"reader" : {}
						  };
		
		if ($("#source-adapter-simple").val() == 'Select Datasource' || 
				$("#source-adapter-tables-simple").val() == 'Select Target Object') {
			validations.errors.push("Datasource or target object missing.");
			return validations;
		}
		
		if (stateJson.reader.property.hasOwnProperty("batchsize")) {
			var bsize = stateJson.reader.property.batchsize;
			if (isNaN(bsize)) {
				validations.errors.push("Trigger Batch Size should be a positive number.");
				return validations;
			} else {
				var bsizeNum = parseInt(bsize);
				if (bsizeNum < 1) {
					validations.errors.push("Trigger Batch Size should be a positive number.");
					return validations;
				}
			}
		}
		
		var filterLogic = $('#filter-logic').val();

		var filterValidation = checkFilterLogic(filterLogic);
		if (filterValidation.errors.length > 0) {
			validations.errors = validations.errors.concat(filterValidation.errors);
			return validations;
		}
		
		var simpleReaderJson = jQuery.extend(true, {}, stateJson.reader); // Copy reader json to a new json object
		simpleReaderJson.name = $("#source-adapter-simple").val();
		simpleReaderJson.property["builder"] = "simple";
		simpleReaderJson.property["table"] = $("#source-adapter-tables-simple").val();
		simpleReaderJson.property["conditions"] = JSON.stringify(getConditions().jsonstring);
		simpleReaderJson.property["filter_logic"] = $('#filter-logic').val();
			
		validations.reader = simpleReaderJson;
		
		return validations;
	}
	
	//function to validate filter logic
	function checkFilterLogic(filterLogic) {
		
		var checkResult = {
				"errors" : [], 
				"filter_logic" : ""
		};
		
		if (filterLogic.trim() == '') {
			return checkResult;
		}
		
		var numberPattern = /\d+/g;
		var numbers = filterLogic.match(numberPattern);
		
		var result = "true";
		for (var ctr=0; numbers != null && ctr<numbers.length; ctr++) {
			if (numbers[ctr] < 1 || numbers[ctr] > readerConditionCounter) {
				checkResult.errors.push("Condition " + numbers[ctr] + " not found.");
			}
		}
		
		var conditions = getConditions();
		
		if (conditions.errors.length > 0) {
			checkResult.errors = checkResult.errors.concat(conditions.errors);
		}
		
		if (checkResult.errors.length > 0) {
			return checkResult;
		}
		
		return checkResult;
	}
	
	//function to fetch conditions from simple query builder
	function getConditions() {
		var conditionListSql = [];
		var conditionListJson = [];
		var conditionListErrors = [];
		
		$('#conditionTableBody tr').each(function(){
			var ctr = $(this).children(".condition").children(".priority").text();
			ctr = ctr.trim();
			
			var field = $('#field-'+ctr).val();
			var readableOp = $('#operation-'+ctr).val();
			var value = $('#value-'+ctr).val();
			
			if (field == 'Select Field' || readableOp == 'Select Operation') {
				conditionListErrors.push('Errors in condition ' + ctr);
			}
			
			var operatorIndex = possibleConditionOperations.readableOperatorList.indexOf(readableOp);
			var condition = field + ' ' + possibleConditionOperations.actualOperatorList[operatorIndex] + ' ' + value ;
			conditionListSql.push(condition);
			
			var cJson = {};
			cJson["field"] = field;
			cJson["operator"] = readableOp;
			cJson["value"] = value;
			
			conditionListJson.push(cJson);
			
		});
		
		var conditionList = {};
		conditionList["sqlstring"] = conditionListSql;
		conditionList["jsonstring"] = conditionListJson;
		conditionList["errors"] = conditionListErrors;
		
		return conditionList;
	}
	
        // Simple function to append new timeline element
        function addRow(tableBody, task) {
        	tableBody.append($("<tr><td class='task' style='vertical-align: middle; width: 5%;' align='center'><span style='font-size: 2em;' class='priority'>" + task.recid + "</span></td><td class='task' style='vertical-align: middle; width: 10%;' align='left'><span id='ruleNameEditable_" + task.recid + "' class='editable-field' style='font-size: 1em;' class='priority'>" + (task.name == "" ? "NewRule" + task.recid : task.name)  + "</span></td><td colspan='2' class='step'>" + '<div class="table-menu"><div class="btn-group" role="group"><button onclick="deleteStep(this);" type="button" title="Delete" class="btn btn-white rowmenubutton row_delete"> <i class="fa fa-trash-o rowmenuicon"></i></button></div></div>' + task.step + "</td></tr>"));
        	
        	var sourcecheck = $('#newTaskTableBody tr:last').find("[id^='sourcecheck-']");
        	var id = $(sourcecheck).attr('id').split('-')[1];
        	
        	loadTargetProps('tc-'+id);
        	
        	loadICheck(sourcecheck);
        	
        	if (task.status == true) {
        		$(sourcecheck).iCheck('check');
        		$('#disabledStatusMessage-'+id).hide();
        		$('#disabledStatusMessage-'+id).siblings().show();
        		
        		loadUpdateSourceProps('su-tc-'+id);
        		
        	} else {
        		$('#statuswriter-'+id+' *').attr('disabled', true);
        		$('#disabledStatusMessage-'+id).show();
        		$('#disabledStatusMessage-'+id).siblings().hide();
        	}
        	
        	$("#ruleNameEditable_" + task.recid).editableFieldFunction();
        	
        	$("#ruleNameEditable_" + task.recid).on('validate', function (event, validationParameter) {
        		var myregexp = /^(\w|-|\/)+$/i;
        		valueToValidation = validationParameter.validatedValue;
        		if(myregexp.test(valueToValidation)) {
        			validationParameter.validationResult = myregexp.test(valueToValidation)
        		} else {
        			validationParameter.errorMessage = "Name can contain only alphanumeric symbols and _-/";
        		}
        	}).on('editsubmit', function (event, newValue) {
        		task.name = newValue;
        	    stateJson.task[task.recid-1].name = newValue;
        	});
        }

        function addTask() {
        	var t = createTask(null);
            t.recid = taskJsonArray.length + 1;
            
            var stateTaskObject = createNewTaskJson(t.recid);
            stateTaskObject.map.sequence = t.recid;
            stateJson.task.push(stateTaskObject);
            
            
            taskJsonArray.push(t);
            addRow($('#newTaskTableBody'), t);
            
            $('.footable').trigger('footable_redraw');
        }

        // Function to delete all conditions from simple query builder
        function clearAllConditions() {
        	
        	var curSrcAdp = $('#source-adapter-simple').val();
     		var curSrcTbl = $('#source-adapter-tables-simple').val();
     		var adpType;
     		
     		if (curSrcAdp == 'Select Datasource' || curSrcTbl == 'Select Table' || readerConditionCounter == 0) {
     			swal("Can't delete conditions","No conditions found.", "error");
     			return;
     		}
     		
        	swal({
    			title : "Clear All Conditions?",
    			showCancelButton : true,
    			confirmButtonColor : "#DD6B55",
    			confirmButtonText : "Yes, delete it!",
    			cancelButtonText : "No, cancel it!",
    			closeOnConfirm : true,
    			closeOnCancel : true
    		}, function(isConfirm) {
    			if (isConfirm) {
    				$('#conditionTableBody').find(".row_delete").each(function(){
    					deleteCondition($(this));
    	    		});
    			}
    		});
        	
        }
        
     	// Simple function to append new timeline element
        function addNewCondition() {
     		
     		var curSrcAdp = $('#source-adapter-simple').val();
     		var curSrcTbl = $('#source-adapter-tables-simple').val();
     		var adpType;
     		
     		if (curSrcAdp == 'Select Datasource' || curSrcTbl == 'Select Table') {
     			swal("Can't add condition","Datasource or Object missing.", "error");
     			readerConditionCounter = 0;
     			return;
     		}
     		
     		if (readerConditionCounter == 0) {
     			$('#conditionTableBody').empty();
     			$('#condition_table span.footable-toggle').hide();
     		}
     		
     		var curSrcCols = [];
     		if ((prevSrcAdp == curSrcAdp) && (prevSrcTbl == curSrcTbl)) {
     			curSrcCols = prevSrcCols;
     		} else {
     			 $.ajax({
         			async: false, 
         			url : curl_link + "getColumnMetadata?readerAdapter=" + curSrcAdp + "&tableName=" + curSrcTbl,
         			dataType : "json"
         		})
         		.done(
         			function(data) {
         				if (data.length > 0) {
         					adpType = getSelectedAdapterType();
         					readerConditionCounter = 0;
         					
         					var colNameList = getLeafNodes(data);
         	               
         					var charToReplace = '/';
         					var re = new RegExp(charToReplace, "g");
         	               
         					for (var ctr=0; ctr<colNameList.length; ctr++) {
        						var newCol = colNameList[ctr];
        						newCol = newCol.substring(newCol.indexOf("/") + 1).replace(re, ".");
        						if (adpType == 'JavaAdapter') {
        							if (newCol.indexOf("params.") >= 0) {
        								newCol = newCol.split('params.')[1];
        								curSrcCols.push(newCol);
        							}
        						} else {
        							curSrcCols.push(newCol);
        						}
         					}
         				}
         			}	
         		);
     			 
     			 if(curSrcCols.length == 0) {
     				readerConditionCounter = 0;
     				return false;
     			 }
     			 
     			 prevSrcAdp = curSrcAdp;
     			 prevSrcTbl = curSrcTbl;
     			 prevSrcCols = curSrcCols;
     		}
     		
     		var rowid = (readerConditionCounter+1);
     		var fieldListDropdown = createDropdown(curSrcCols, "Select Field", '', "field-"+rowid, true);
     		var conditionOperationDropdown = createDropdown(possibleConditionOperations.readableOperatorList, "Select Operation", '', "operation-"+rowid, true);
     		
     		var extraVariables = '';
     		for (var ctr=0; ctr<adapterTablesJson.length; ctr++) {
            	if (adapterTablesJson[ctr].name == curSrcAdp) {
            		
            		var adpType = adapterTablesJson[ctr].type;
            		if (adpType == 'SalesforceAdapter') {
            			extraVariables = 	"<li class='divider'></li>" + 
											"<li class='extra-variables variables'><a>$profile.Salesforce.Id</a></li>";
            		} else {
            			extraVariables = '';
            		}
            		break;
            	}
            }
     		
     		var valueInputElementHTML = "<div class='input-group'>" + 
     										"<input class=\"form-control\" type=\"text\" id=\"value-" + rowid + "\" />" + 
     										"<div class='input-group-btn'>" +
     											"<button title='Variables' data-toggle='dropdown' class='btn btn-white dropdown-toggle form-control' type='button'>" + 
     												"<span class='align-button-text'>(x)</span>" +
     											"</button>" +
     											"<ul class='dropdown-menu pull-right'>" + 
	     											"<li class='variables'><a>$last_success_run</a></li>" + 
	     											"<li class='variables'><a>$last_success_start_run</a></li>" + 
	     											"<li class='variables'><a>$last_run_start_time</a></li>" + 
	     											"<li class='variables'><a>$last_completed_run</a></li>" +
	     											extraVariables + 
	     										"</ul>" + 
	     									"</div>" + 
	     								"</div>";
        	
     		$("#conditionTableBody").append($('<tr>').html("<td class=\"condition\"><span class=\"priority\" style=\"font-size: x-large;\">&nbsp;&nbsp;" + rowid + "</span></td><td>" + fieldListDropdown + "</td><td>" + conditionOperationDropdown + "</td><td>" + '<div class="table-menu"><div class="btn-group" role="group"><button onclick="deleteCond(this);" type="button" title="Delete" class="btn btn-white rowmenubutton row_delete"> <i class="fa fa-trash-o rowmenuicon"></i></button></div></div>' + valueInputElementHTML + "</td>"));
        	$('#condition_table').trigger("chosen:updated");
        	$('.footable').trigger('footable_redraw');
        	
        	if (rowid == 1) {
        		$("#filter-logic").val("1");
        	} else {
        		$("#filter-logic").val($("#filter-logic").val() + " and " + rowid);
        	}
        	
        	readerConditionCounter += 1;
        }
        
        // function to get column names from metadata json
        function getLeafNodes(metadatajson) {
        	var childList = [];

        	for (var ctr=0; ctr < metadatajson.length; ctr++) {
        		if (metadatajson[ctr].hasOwnProperty('children')) {
        			childList = childList.concat( getLeafNodes(metadatajson[ctr].children) );
        		} else {
        			childList.push(metadatajson[ctr].data.val);
        		}
        	}
 
        	return childList;
        }

        //Sweet alert for Deletes a condition
        function deleteCond(row) {
        	
    		swal({
    			title : "Delete Condition " + parseInt($(row).closest("td").siblings("td").children(".priority").text()) + "?",
    			showCancelButton : true,
    			confirmButtonColor : "#DD6B55",
    			confirmButtonText : "Yes, delete it!",
    			cancelButtonText : "No, cancel it!",
    			closeOnConfirm : true,
    			closeOnCancel : true
    		}, function(isConfirm) {
    			if (isConfirm) {
    				deleteCondition(row);
    			}
    		});
    	}
        //Deletes a condition
    	function deleteCondition(row) {
    	    var count = 0;
    		var conditionCtr = parseInt($(row).closest("td").siblings("td").children(".priority").text());
    		$(row).closest("tr").remove();
    		
    		var ctr = 1;
    		$('#conditionTableBody tr').each(function(){
    			$(this).children(".condition").children(".priority").text(ctr);
    			$(this).find("[id^='field-']").attr("id", "field-"+ctr);
    			$(this).find("[id^='operation-']").attr("id", "operation-"+ctr);
    			$(this).find("[id^='value-']").attr("id", "value-"+ctr);
    			ctr++;
    		});
    		readerConditionCounter = ctr-1;
    		
    		if (readerConditionCounter == 0) {
    			$("#conditionTableBody").append(defaultConditionRow);
    			$('#condition_table span.footable-toggle').hide();
     		}
    		
    		var fLogic = $("#filter-logic").val();
    		var filterText01 = "and " + conditionCtr + ' ';
    		var filterText02 = "and " + conditionCtr;
    		var filterText03 = "or " + conditionCtr + ' ';
    		var filterText04 = "or " + conditionCtr;
    		var filterText05 = conditionCtr + ' and ';
    		var filterText06 = conditionCtr + ' or ';
    		
    		if (fLogic.indexOf(filterText01) >= 0) {
    			fLogic = fLogic.replace(filterText01, '');
    		} else if (strEndsWith(fLogic,filterText02)) {
    			fLogic = fLogic.replace(filterText02, '');
    		} else if (fLogic.indexOf(filterText03)  >= 0) {
    			fLogic = flogic.replace(filterText03, '');
    		} else if (strEndsWith(fLogic,filterText04)) {
    			fLogic = fLogic.replace(filterText04, '');
    		} else if (new RegExp("^" + filterText05, "g").test(fLogic)) {
    		     if(fLogic =="1 and 2" && filterText01 == "and 1 "){
    		     	fLogic = "1";
    		     	count++;
    		     }
    		     else{
    				fLogic = fLogic.replace(filterText05, '');
    				}
    		} else if (new RegExp("^" + filterText06, "g").test(fLogic)) {
    			fLogic = fLogic.replace(filterText06, '');
    		}
    		
    		$("#filter-logic").val(fLogic);
    		
    		var numberPattern = /\d+/g;
    		var numbers = fLogic.match(numberPattern);
    		
    		if(numbers != undefined){
	    		for (var temp=0; temp<numbers.length; temp++) {
	    			if (numbers[temp] >= conditionCtr) {
	    				if (new RegExp("^" + numbers[temp] + " ", "g").test(fLogic)) {
	    					fLogic = fLogic.replace(numbers[temp]+' ', (numbers[temp]-1)+' ');
	    				} else if (strEndsWith(fLogic,' ' + numbers[temp])) {
	    					fLogic = fLogic.replace(' ' + numbers[temp], ' ' + (numbers[temp]-1));
	    				} else {
	    					fLogic = fLogic.replace(' ' + numbers[temp] + ' ', ' ' + (numbers[temp]-1) + ' ' );
	    				}
	    			}
	    		}
    		}
    		
    		if(fLogic == 0)
    		$("#filter-logic").val("");
    		  else if(fLogic == 1 &&  conditionCtr == 1 && count == 0)
    		       $("#filter-logic").val("");
				else
					$("#filter-logic").val(fLogic);
    		Event.stopImmediatePropagation;
    	}
        
    	function buildQueryForSimple(){

    		var queryObj = {};
    		queryObj.adapterName = $("#source-adapter-simple").val();
    		queryObj.tableName = $("#source-adapter-tables-simple").val();
    		queryObj.queryLogic = $("#filter-logic").val();
    		queryObj.queryParam = [];
    		
    		var rowCount = $('#condition_table tr').length;
    		for(i = 1; i < rowCount; i++){
    			if($("#field-"+ i).val() == undefined){
    				continue;
    			}
    			var queryParamObj = {};
    			queryParamObj.field = $("#field-"+ i).val();
    			queryParamObj.operator = $("#operation-"+ i).val();
    			queryParamObj.value = $("#value-"+ i).val();
    			
    			queryObj.queryParam.push(queryParamObj);
    		}
    		
    		var result = { "errors" : []};
    		$.ajax({
    			async :false,
    			method: "POST",
    			url : curl_link + "buildQuery",
    			data: {query : JSON.stringify(queryObj)},
    			dataType : "json"
    		}).done(function(data) {
    			if(data.hasOwnProperty("error")){
    				result.errors.push(data.error);
    			}
    			if(data.hasOwnProperty("query")){
    				result.sql = data.query;
    			}
    		}).fail(function(data) {
    			result.errors.push("Error in building query");
    		});
    		return result;
    	}
    	
    	function executeSQL() {
    		
    		$('#simpleQueryResultTable thead tr').remove();
        	$('#simpleQueryResultTable tbody tr').remove();
        	var readerJSON = validateSimpleTrigger();
        	
        	if($("#source-adapter-tables-simple").val() == "<Regular Expression Pattern>" && getSelectedAdapterType().toLowerCase() == "ftpadapter" &&
        		($("#readerprop-regularexp").val() == null ||
        		$("#readerprop-regularexp").val() == "")) {
        		swal("Can't execute query","Please provide Regular Expression in Properties tab!", "error");
        	}
        	
        	if (readerJSON.errors.length > 0) {
        		swal("Can't execute query","Please check query again.", "error");
        		return;
        	}

        	var result = buildQueryForSimple();
        	var sql = result.sql;
        	readerJSON.reader.property.query = sql;
        	
        	var readerObj = JSON.stringify(stateJson.reader);
        	
        	$.ajax({
    			method: "POST",
    			url : curl_link + "/executeSql",
    			data: { query: sql, readerAdapter: prevSrcAdp, readerObj : readerObj },
    			dataType : "json"
    		})
    		.done(function(xmlData) {
    			var status = xmlData.isResultNested;
    			var excepn=xmlData.isException1;
    			
    			if(status) {
    				constructResultSimple(xmlData.resultJsonArray,excepn);
    			} else {
    				constructTableSimple(xmlData,excepn);
    			}
    			
    			if (!$("#result-table").hasClass("in")) {
            		$('#result-anchor').trigger("click");
            	}
    			
    		})
    		.fail(function() {
    			if (!$("#result-table").hasClass("in")) {
            		$('#result-anchor').trigger("click");
            	}
    		});
        	
        }
    	
    	function constructResultSimple(data,isexcepn) {
        	$("#simpleQueryResultTable").hide();
        	var finalText = "";
        	var jsData = JSON.parse(data);
        	
        	for(var i=0; i<jsData.length; i++) {
    			var rowText = jsData[i].name;
    			var xmlText = jsData[i].fulltext;
    			
    			xmlText = xmlText.replace(/\</g, "&lt;").replace(/\>/g, "&gt;");
    			
    			finalText += "<ul>";
    			finalText += "<li>" + "<i id='resultrecord-simple-" + i + "' class='icon123 fa fa-plus-square-o' onclick='createViewSimple(this);'></i> " + rowText + "</li>";
    			finalText += "<li class='fullText'><pre style='border: none; border-radius: 0; background-color: #f3f3f4;'>" + xmlText + "</pre></li>";
    			finalText += "</ul>";
        	}
        	
        	$("#resultDivSimple").html(finalText);
        	
        	if (data.length == 0 && isexcepn != true) {
        	  $("#resultDivSimple").html("<span class='noresultmsg'>No result to display.</span>");
        	}
        	if (data.length == 0 && isexcepn == true) {
        	  $("#resultDivSimple").html("<span class='noresultmsg'>Error in the Query Please check your query again.</span>");
        	}
        	
        	if (jsData.length > 0) {
        		for (var ctr=0; ctr<jsData.length; ctr++) {
        			createViewSimple($("#resultrecord-simple-"+ctr));
        		}
        	}
        }
        
    function constructTableSimple(tableData,isexcepn) {
    	    $("#simpleQueryResultTable").show()  
    	    var empty="";
    	    $("#resultDivSimple").html(empty);
        	$("#default-tr-simple").hide();
        	$("#defaultrow-simple").hide();
        	
        	var theadArray = JSON.parse(tableData.header);
        	var tbodyArray = JSON.parse(tableData.body);
        	
        	var newTr = $('<tr></tr>');
        	
        	$.each(theadArray, function(count, item) {
        		
        		var newTh = $('<th></th>').html(item);
        		
        		newTr.append(newTh);
        	    
        	});
        	
        	$('#simpleQueryResultTable thead').append(newTr);
        	
        	$.each(tbodyArray, function(count, row) {
        		var newTr = $('<tr></tr>');
        		
        		$.each(row, function(count1, value) {
        			var newTd = $('<td></td>').html(value);
        			newTr.append(newTd);
        		});
        	    
        	    $('#simpleQueryResultTable tbody').append(newTr);
        	});
        	
        	if (tbodyArray.length == 0 && isexcepn != true) {
        		var newTr = $('<tr id="defaultrow-simple"></tr>');
        		var newTd = $('<td></td>').html('No result to display.');
        		newTr.append(newTd);
        		$('#simpleQueryResultTable tbody').append(newTr);
        	}
        	
        	if (tbodyArray.length == 0 && isexcepn == true) {
        		var newTr = $('<tr id="defaultrow-simple"></tr>');
        		var newTd = $('<td></td>').html('Error in the Query Please check your query again.');
        		newTr.append(newTd);
        		$('#simpleQueryResultTable tbody').append(newTr);
        	}
        	 $('#simpleQueryResultTable').footable();
        	 $('#simpleQueryResultTable').trigger('footable_initialized');
        	 $('#simpleQueryResultTable').trigger('footable_resize');
        	 $('#simpleQueryResultTable').trigger('footable_redraw');
        	 $('#simpleQueryResultTable .footable-page a').filter('[data-page="0"]').trigger('click');
        }
    
    function createViewSimple(id) {
        $(id).parent().parent().children('.fullText').toggle();
        
        if($(id).hasClass('fa-plus-square-o')) {
        	$(id).removeClass('fa-plus-square-o');
        	$(id).addClass('fa-minus-square-o');
        } else if($(id).hasClass('fa-minus-square-o')) {
        	$(id).removeClass('fa-minus-square-o');
        	$(id).addClass('fa-plus-square-o');
        }
    }
    
	$(document).ready(function() {
		
		//Helper function to keep table row from collapsing when being sorted     
		var fixHelperModified = function(e, tr) { 
			var $originals = tr.children(); 
			var $helper = tr.clone(); 
			$helper.children().each(function(index) { 
				$(this).width($originals.eq(index).width())}); 
			return $helper;
		};     
		
		//Make diagnosis table sortable     
		$("#new_table tbody").sortable({ 
			helper: fixHelperModified, 
			start: function(e, ui){		
		        ui.helper.hide()		
		    }, 
			stop: function(event,ui) {
				renumber_table('#new_table tbody')
			}     
		}).disableSelection();     
	}); 
	
	//Renumber table rows 
	function renumber_table(tableID) { 

		$(tableID + " tr").each(function() { 
			var newValue = $(this).parent().children().index($(this)) + 1; 
			
			var oldValue = $(this).find('.priority').text();
			
			$(this).find('.priority').html(newValue);     
			
			stateJson.task[oldValue-1].map.sequence = newValue;
			
			taskJsonArray[oldValue-1].recid = newValue;
		}); 
		
		// Reorder master json
		stateJson.task.sort(function(a, b) {
		    return parseFloat(a.map.sequence) - parseFloat(b.map.sequence);
		});
		
		// Re0rder taskjson
		taskJsonArray.sort(function(a, b) {
		    return parseFloat(a.recid) - parseFloat(b.recid);
		});
		
	}

	function activeMenu(row, value) {
		
	    var row$ = $(row);
	    var menu$ = $(row$.find(".table-menu"));
	    
	    if (value) {
	        row$.addClass("selectedRow");
	        menu$.addClass("table-menu-active");
	        menu$.attr("visibility", "visible");
	    } else {
	        row$.removeClass("selectedRow");
	        menu$.removeClass("table-menu-active");
	        menu$.attr("visibility", "hidden");
	    }
	}
	
	function isMenuActived(row$) {
	    return $(row$).find(".table-menu-active").length > 0;
	}
	
	function isActivableMenu(row$) {
	    return $(row$).find(".table-menu").length > 0;
	}
	
	//Deletes a task
	function rowDelete(row) {
		var recIdToRemove = parseInt($(row).closest("td").siblings("td").children(".priority").text());
		taskJsonArray.splice(recIdToRemove-1, 1);
		$(row).closest("tr").remove();
		
		for (var ctr=0; ctr<taskJsonArray.length; ctr++) {
			taskJsonArray[ctr].recid = ctr+1;
			$($('#newTaskTableBody tr')[ctr]).children(".task").children(".priority").text(ctr+1);
		}
		
		
		stateJson.task.splice(recIdToRemove-1, 1);
		for (var ctr=0; ctr<stateJson.task.length; ctr++) {
			stateJson.task[ctr].map.sequence = ctr+1;
		}
		
		Event.stopImmediatePropagation;
	}
	
	function deleteStep(row) {
		swal({
			title : "Delete Rule " + parseInt($(row).closest("td").siblings("td").children(".priority").text()) + "?",
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			confirmButtonText : "Yes, delete it!",
			cancelButtonText : "No, cancel it!",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function(isConfirm) {
			if (isConfirm) {
				rowDelete(row);
			}
		});
	}
	
	function validateMapInputs(task, writerMap) {
		var isValid = true;
        if (writerMap == "writermap") {
        	
        	var readerDS = "";
        	var readerQ = "";
        	if ($("#simpleQueryBuilder").hasClass("active")) {
        		readerDS = $("#source-adapter-simple").val();
        		var result = buildQueryForSimple();
        		if(result.sql != undefined){
        			readerQ = result.sql;
        		}
    		} else {
    			readerDS = $("#source-adapter-advance").val();
    			readerQ = $("#readOnlyQuery").text();
    		}
        	
        	var writerDS = task.writer.name;
        	var writerTable = task.writer.property.table;
        	
        	if (readerDS.trim() == "" || readerDS.trim() == "Select Datasource" || readerQ.trim() == "" 
        		|| writerDS.trim() == "" || writerDS.trim() == "Select Target Connector" 
        			|| writerTable.trim() == "" || writerTable.trim() == "Select Target Object") {
        		
        		isValid = false;
        	}
        	
        } else if (writerMap == "writermap-success" || writerMap == "writermap-failure") {
        	
        	var swReaderDS = task.writer.name;
        	var swReaderTable = task.writer.property.table;
        	var swWriterDS = task.statusWriter.name;
        	var swWriterTable = task.statusWriter.property.table;
        	
        	if (swReaderDS.trim() == "" || swReaderDS.trim() == "Select Target Connector" 
        		|| swReaderTable.trim() == "" || swReaderTable.trim() == "Select Target Object" 
        			|| swWriterDS.trim() == "" || swWriterDS.trim() == "Select Update Target" 
        				|| swWriterTable.trim() == "" || swWriterTable.trim() == "Select Target Object") {
        		
        		isValid = false;
        	}
        	
        }
        
        return isValid;
	}
	
	$(document).ready(function(){
		
		 // On click of any map link in the rules listing, this method opens map in a modal window
		$('#new_table').on('click','a[data-target=#mapWindow]', function(ev) {
	        ev.preventDefault();
	        
	        var bootStrapEnv = findBootstrapEnvironment();
	        
	        if(bootStrapEnv == "xs") {
	        	swal("Can't open map","Mapping is not supported on Mobile Devices", "info");
	        	return false;
	        }

	        if($(this).attr("disabled") == "disabled") {
	        	return false;
	        }
	        
	        if(seqNo != 'null') {
	        	mapRowSequence = parseInt(seqNo);
	        	// Making seqNo as null again, so, when we want to open map for the next time,
	        	// it should open normally and should not consider seqNo
	        	seqNo = 'null';
	        } else {
	        	mapRowSequence = parseInt($(this).closest("tr").children('.task').children('.priority').html());
	        }
	        
	        var task = stateJson.task[mapRowSequence - 1];

	        if(mapType != 'null') {
	        	writerMapType = mapType;
	        	mapType = 'null';
	        } else {
	        	writerMapType = $(this).data("maptype");
	        }

	        var operation = "";
			var table = "";
	        
	        // If it is a Status Writer Map
	        if(writerMapType == "writermap-success" || writerMapType == "writermap-failure") {
	        	// If Status Writer is a Console Adapter 
	        	if(task.statusWriter.name.trim().toLowerCase() == "consoleadapter") {
	        		operation = "Insert";
					table = "Console";
	        	} 
	        	// Validate the Status Writer Map
	        	else if(!validateMapInputs(task, writerMapType)) {
	        		swal("Can't open map.","Source or Target missing.", "error");
		        	return false;
	        	} else {
	        		operation = task.statusWriter.name;
					table = task.statusWriter.property.table;
	        	}
	        }
	        // If it is a Writer Map, Validate the Map
	        else if(!validateMapInputs(task, writerMapType)) {
	        	swal("Can't open map.","Source or Target missing.", "error");
	        	return false;
	        }
	        
	        var target = curl_link + "/project/map.m?mapId=";
	        
			if(writerMapType == "writermap") {
	        	target += task.map.id + "&writerName="+ task.writer.name + "&tableName="+ task.writer.property.table;
	        } else if(writerMapType == "writermap-success") {
	        	target += task.statusWriter.onSuccess.map.id + "&writerName="+ operation + "&tableName="+ table;
	        } else if(writerMapType == "writermap-failure") {
	        	target += task.statusWriter.onFailure.map.id + "&writerName="+ operation + "&tableName="+ table;
	        }
	        
	        $("#mapWindow .modal-title").html("Mapping Screen");
	        
	        // load the url and show modal on success
	        $("#mapWindow .modal-body").load(target, function() { 
	             $("#mapWindow").modal("show");
	        });
	    });
		 
		 
		// open advance query builder when clicking on "switch to advance"
		$("a[data-target=#querybuilder]").click(function(ev) {
			ev.preventDefault();
			
			if ($("#source-adapter-advance").val().toLowerCase() == "select datasource") {
				swal("Can't open Query Builder.","Datasource missing", "error");
				return false;
			}
			
			var target = $(this).attr("href");
			target = encodeURI(target);

			$("#querybuilder .modal-title").html("Advanced Query Builder");
			
			// load the url and show modal on success
			$("#querybuilder .modal-body").load(target, function() { 
				 $("#querybuilder").modal("show");
			});
		});
		
		$("div .tabs-container").on("click", "#configure" , function(ev) {
			ev.preventDefault();
			
			var targetUrl = curl_link + "configureXMLFormatter";

			$("#xmlFormatter .modal-title").html("Configure XML Formatter");
			
			// load the url and show modal on success
			$("#xmlFormatter .modal-body").load(targetUrl, function() { 
				 $("#xmlFormatter").modal("show");
			});
		});
	});
		
	function saveXMLFormatter() {
		var xmlString = "";
		var lines = "";
		var drivingColumn = "";
		
		var listItems = $("#columnsList li");
		
   		for(var idx=0; idx<listItems.length; idx++) {
   			
   			if( !($("#columnsList li").eq(idx).find("span.sourceCol").hasClass("driver-col-indent")) && 
   					!($("#columnsList li").eq(idx).find("span.sourceCol").hasClass("lines-indent"))) {
   				
   				if(xmlString != "") {
   					xmlString += ",";
   				}
   				
   				xmlString += $("#columnsList li").eq(idx).find("span.sourceCol").text();
   				
   			} else if($("#columnsList li").eq(idx).find("span.sourceCol").hasClass("driver-col-indent")) {
   			
   				drivingColumn = $("#columnsList li").eq(idx).find("span.sourceCol").text();
   			
   			} else {
   				
   				if(lines != "") {
   					lines += ",";
   				}
   				
   				lines += $("#columnsList li").eq(idx).find("span.sourceCol").text();
   			}
   		}
   		
   		if(drivingColumn.trim() == "") {
   			$("#errors").text("Please Select the Driving Column");
   			$("#errors").show();
   			return;
   		}
   		
   		if(lines.trim() == "") {
   			$("#errors").text("Atleast one Line Item should be Present");
   			$("#errors").show();
   			return;
   		}
   		
   		if(xmlString != "") {
   			xmlString += ",";
   		}
   		
   		xmlString += drivingColumn + ",[" + lines + "]"; 
   		
   		$("#xmlFormatter").modal('toggle');
		$("#readerprop-xmlformatter").val(xmlString);
		stateJson.reader.property["xmlformatter"] = xmlString;
		xmlFormatter.query = stateJson.reader.property.query;
	}
		
	function saveQueryConfirmation() {
		errorMsg = '';
		
		swal({
			title : "Save the Query?",
			text : "",
			showCancelButton : true,
			confirmButtonColor : "#6dc066",
			confirmButtonText : "Yes, save it!",
			cancelButtonText : "No, cancel it!",
			closeOnConfirm : false,
			closeOnCancel : true
		}, function(isConfirm) {
			if (isConfirm) {
				saveQueryToFile();
			}
		});
	}

	function saveQueryClose() {
		errorMsg = '';
		
		swal({
			title : "Closing Query Builder",
			text : "Press Esc or Click Outside to Canccel",
			showCancelButton : true,
			confirmButtonColor : "#6dc066",
			confirmButtonText : "Exit, With Save!",
			cancelButtonText : "Exit, Without Save!",
			allowOutsideClick : true,
			closeOnConfirm : true,
			closeOnCancel : true
		}, function(isConfirm) {
			if (isConfirm) {
				saveQueryToFile();
				swal("Saved!", "Query Saved", "success");
				$("#querybuilder").modal('toggle');
			}
			else{
				$("#querybuilder").modal('toggle');
				
			}
		});
	}

	function closeModal(){
		$("#queryeditor").val($("#readOnlyQuery").text());
		$("#querybuilder").modal('toggle');
	}

	function tempSaveAdvQuery(){
		
		var sql = $("#queryeditor").val();
		
		// Dont execute query if quickbooks desktop is reader
		for (var ctr=0; ctr<adapterTablesJson.length; ctr++) {
        	if (adapterTablesJson[ctr].name == $('#source-adapter-advance').val()) {
        		
        		var adpType = adapterTablesJson[ctr].type;
        		if (adpType == 'QuickBooksAdapter2') {
        			$("#querybuilder").modal('toggle');
    				$("#readOnlyQuery").text(sql);
    				return;
        		}
        	}
        }
		
		
		if (sql.toLowerCase().indexOf('$profile.salesforce.id') >= 0) {
			$("#querybuilder").modal('toggle');
			$("#readOnlyQuery").text(sql);
			return;
		}
		
		var readerObj = JSON.stringify(stateJson.reader);
		
		var isQueryValid = "true";
		$.ajax({
			method: "POST",
			url : curl_link + "/executeSql",
			data: { query: sql, readerAdapter: readerAdapter, readerObj : readerObj },
			dataType : "json"
		})
		.done(function(xmlData) {
			var status = xmlData.isResultNested;
			var excepn=xmlData.isException1;
			if (excepn == true) {
				isQueryValid = "false";
				swal("Query not saved",'Please check your query again.', "error");
			}
			
			if (isQueryValid == "true") {
				$("#querybuilder").modal('toggle');
				$("#readOnlyQuery").text(sql);
			}
		})
		.fail(function() {
			if (isQueryValid == "true") {
				$("#querybuilder").modal('toggle');
				$("#readOnlyQuery").text(sql);
			}
		});
		if(isQueryValid == "true"){
			xmlFormatter.isQueryChanged = true;
		}
		
	}
	
	
function validateTask() {
		
		var flowValidationArray = [];
		
		if(stateJson != null) {
			
			var taskArray = stateJson.task;
			
			if(taskArray.length < 1) {
				msg = "<div>Please add atleast one rule in the rules section</div>";
				flowValidationArray.push(msg);
			} else {
				for(var i=0; i<taskArray.length; i++) {
					
					var writer = taskArray[i].writer;
					var writerName = writer.name;
					var validators = writer.validator;
					var properties = writer.property;
					var writerType = properties.writetype;
					var writerTable = properties.table;
					
					var map = taskArray[i].map;
					var writerMap = map.id;
					var writerMapLocation = map.property.location;
					var writerSequence = map.sequence;
					var autocreateMap = map.autocreate;
					
					var hasStatusWriter = taskArray[i].hasOwnProperty("statusWriter");
					
					var ruleErrorHTML = "<div>Errors in rule " + writerSequence + "<ul>";
					var hasError = false;
					
					validators = [];
					for (var prop in validators) {
						var validatorMethod = window[validators[prop]];
						var error = validatorMethod(writer, prop);
						if (error && error.length > 0) {
							hasError = true;
							ruleErrorHTML += '<li>' + prop + ': ' + error + '</li>';
						}
					}
					
					if(isEmptyOrNullOrDefault(writerName, "Select Datasource")) {
						hasError = true;
						ruleErrorHTML += '<li>Target Connector Missing</li>';
					}

					if (isEmptyOrNullOrDefault(writerType, "Select Operation")) {
						hasError = true;
						ruleErrorHTML += '<li>Operation Missing</li>';
					}
					
					if (isEmptyOrNullOrDefault(writerTable, "Select Target Object")) {
						hasError = true;
						ruleErrorHTML += '<li>Target Object Missing</li>';
					}
					
					if ((isEmptyOrNullOrDefault(writerMap, null) || isEmptyOrNullOrDefault(writerMapLocation, null)) && !autocreateMap=='false') {
						hasError = true;
						ruleErrorHTML += '<li>Target Map Missing</li>';
					}

					if (hasStatusWriter) { 
						
						var swValidation = isValidStatusWriter(taskArray[i].statusWriter);
						
						if (!swValidation.result) {
							hasError = true;
							ruleErrorHTML += swValidation.errorMsg;
						}
						
						var swValidators = taskArray[i].statusWriter.validator;
						
						swValidators = [];
						for (var prop in swValidators) {
							var validatorMethod = window[swValidators[prop]];
							var error = validatorMethod(taskArray[i].statusWriter, prop);
							if (error && error.length > 0) {
								hasError = true;
								ruleErrorHTML += '<li>' + prop + ': ' + error + '</li>';
							}
						}
						
					}
					
					if (hasError) {
						ruleErrorHTML += '</ul></div>';
						msg = ruleErrorHTML;
						flowValidationArray.push(msg);
					}
						
				}
			}
		}
		
		return flowValidationArray;
	}
	
	function isValidStatusWriter(stWriter) {
		var writerType;
		var writerTable;
		var writerName = stWriter.name;
		var isNotConsole = writerName.trim().toLowerCase() != "consoleadapter";
		if(!isNotConsole){
			 writerType = '';
			 writerTable = '';
		}
		if(isNotConsole){
		 writerType = stWriter.property.writetype;
		 writerTable = stWriter.property.table;
		}
		var mapSuccess = stWriter.onSuccess.map.id;
		var mapLocationSuccess = stWriter.onSuccess.map.property.location;
		
		var mapFailure = stWriter.onFailure.map.id;
		var mapLocationFailure = stWriter.onFailure.map.property.location;
		
		var externalId = stWriter.property.externalid;
		
		var errorListHTML = '';
		
		var hasError = false;
		
		if(isEmptyOrNullOrDefault(writerName, "Select Update Target")) {
			hasError = true;
			errorListHTML += '<li>Update Source - Target Missing</li>';
		}
		
		if( isNotConsole && isEmptyOrNullOrDefault(writerType, "Select Operation")) {
			hasError = true;
			errorListHTML += '<li>Update Source - Operation Missing</li>';
		}
		
		if( isNotConsole && isEmptyOrNullOrDefault(writerTable, "Select Target Object")) {
			hasError = true;
			errorListHTML += '<li>Update Source - Object Missing</li>';
		}
		
		if(isNotConsole && (isEmptyOrNullOrDefault(mapSuccess, null) || isEmptyOrNullOrDefault(mapLocationSuccess, null))) {
			hasError = true;
			errorListHTML += '<li>Update Source - On Success Map Missing</li>';
		}
		
		if(isNotConsole && (isEmptyOrNullOrDefault(mapFailure, null) || isEmptyOrNullOrDefault(mapLocationFailure, null))) {
			hasError = true;
			errorListHTML += '<li>Update Source - On Failure Map Missing</li>';
		}
			
		var returnObj = {};
		returnObj["result"] = !hasError;
		returnObj["errorMsg"] = errorListHTML;
		
		return returnObj;
	}
	
	
	function isEmptyOrNullOrDefault(prop, defaultVal) {
		if(prop == null || prop.trim() == "" || (defaultVal != null && prop.trim() == defaultVal)) {
			return true;
		} 
		
		return false;
	}
	
	function strEndsWith(str, suffix) {
	    return str.match(suffix+"$")==suffix;
	}
	
	// Simple function to append new timeline element for Java Adapter
    function addNewConditionForJavaAdptet() {
 		var curSrcAdp = $('#source-adapter-simple').val();
 		var curSrcTbl = $('#source-adapter-tables-simple').val();

    	var adpType;
 		if (curSrcAdp == 'Select Datasource' || curSrcTbl == 'Select Table') {
 			swal("Can't add condition","Datasource or Object missing.", "error");
 			readerConditionCounter = 0;
 			return;
 		}
 		
 		var curSrcCols = [];
		prevSrcTbl = curSrcTbl;
		 $.ajax({
 			async: false, 
 			url : curl_link + "getColumnMetadata?readerAdapter=" + curSrcAdp + "&tableName=" + curSrcTbl,
 			dataType : "json"
 		})
 		.done(
 			function(data) {
 				if (data.length > 0) {
 					adpType = getSelectedAdapterType();
 					var colNameList = getLeafNodes(data);
 	               
 					var charToReplace = '/';
 					var re = new RegExp(charToReplace, "g");
 	               

     				for (var ctr = 0; ctr < colNameList.length; ctr++) {
						var newCol = colNameList[ctr];
						newCol = newCol.substring(newCol.indexOf("/") + 1).replace(re, ".");
						if (adpType == 'JavaAdapter') {
							if (newCol.indexOf("params.") >= 0) {
								newCol = newCol.split('params.')[1];
								curSrcCols.push(newCol);
							}
						} else {
							curSrcCols.push(newCol);
						}
					}
 					if(newCol.length > 0 && adpType == 'JavaAdapter'){
 						$('#conditionTableBody').empty();
 					}
 				}
 			}	
 		);
		 
		 prevSrcAdp = curSrcAdp;
		 prevSrcTbl = curSrcTbl;
		 prevSrcCols = curSrcCols;

		for(var ctr1=0; ctr1<curSrcCols.length; ctr1++) {
			addNewCondition();
			
			$("#field-"+(ctr1+1)).val(curSrcCols[ctr1]);
			$("#operation-"+(ctr1+1)).val("equals");
		}
    }
    
    function getSelectedAdapterType(){
    	var adpType;
    	for (var ctr=0; ctr<adapterTablesJson.length; ctr++) {
        	if (adapterTablesJson[ctr].name == $('#source-adapter-advance').val()) {
        	 adpType = adapterTablesJson[ctr].type;
        	 return adpType;
        	}
        }
    }
    
    function findBootstrapEnvironment() {
        var envs = ['xs', 'sm', 'md', 'lg'];

        var $el = $('<div>');
        $el.appendTo($('body'));

        for (var i = envs.length - 1; i >= 0; i--) {
            var env = envs[i];

            $el.addClass('hidden-'+env);
            if ($el.is(':hidden')) {
                $el.remove();
                return env;
            }
        }
    }
