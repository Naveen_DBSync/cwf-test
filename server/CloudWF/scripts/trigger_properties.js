
function loadTriggerProperties(isFirstLoad) {
		
		$('.no_ds_msg').hide();
		
		$('#trigger_props').html('');
		
		if (stateJson.reader.name == 'Select Datasource' && (!isFirstLoad)) {
			$('.no_ds_msg').show();
			return;
		}

		var readerPropJson = '';
		
		$.ajax({
  			async: false, 
  			method: "POST",
  			url : curl_link + "getReaderProperties",
  			data: { adapterName: stateJson.reader.name},
  			dataType : "json"
  		})
  		.done(
  			function(result) {
  				readerPropJson = result;
  			}
  		);
		
		var propCount = 0;
		
		var propHTML = "<ul>";
		

		for ( var key in readerPropJson) {

			if (readerPropJson.hasOwnProperty(key)) {

				var propName = key;

				if (propName.toLowerCase() == 'query') {
					continue;
				}

				var simpleUIJson = readerPropJson[key];

				var propDisplayName = propName.trim();
				if (simpleUIJson.displayName.trim() != '') {
					propDisplayName = simpleUIJson.displayName.trim();
				}

				var propValue = (stateJson.reader.property.hasOwnProperty(propName)) ? stateJson.reader.property[propName] : "";
				
				if(propValue == "" && simpleUIJson.hasOwnProperty("defaultValue")) {
					propValue = simpleUIJson.defaultValue.trim();
				}
				
				stateJson.reader.property[propName] = propValue;

				var isXMLFormatter = propDisplayName == 'XML Formatter' ? true : false;
				
				propHTML += "<li class='row'>" + 
								"<label class='control-label col-lg-2'>" + 
									propDisplayName + 
								"</label>" + 
								"<div class='col-lg-4'>" + 
									"<input type='text' class='form-control' id='readerprop-" + propName + "' value='" + propValue + "'/>" + 
								"</div>" + 
								(isXMLFormatter == true ? "<div id='configureDiv' class='col-lg-4'><a id=\"configure\" data-target=\"configure\" style=\"text-decoration: underline; cursor: pointer;\">Configure</a></div>" : "") +
							"</li>";

				propCount++;
			}
		}
		
		propHTML += "</ul>";
		
		if (propCount == 0) {
			propHTML = 'No additional properties required for ' + stateJson.reader.name + '.';
		}
		
		$('#trigger_props').html(propHTML);
	}