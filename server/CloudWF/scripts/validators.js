function columnExists(writer, propKey) {
	var value = writer.property[propKey];

	if (!value || value.length == 0) {
		return 'Empty column value.';
	}

	var metadata = '';
	$.ajax({
		async : false,
		url : curl_link + "map/getMetaData",
		data : {
			"adapterName" : currentTask.writer.name,
			"metaDatatype" : "schema",
			"query" : currentTask.writer.property.table
		},
		dataType : "json"
	}).done(function(data) {
		metadata = data.metadata;
	});

	for (var i = 0; i < metadata.length; i++) {
		if (value == metadata[i].text) {
			return '';
		}
	}
	return 'Column \'' + value + '\' does not exist in target table.';
}

function alphabetic(writer, propKey) {
	var value = writer.property[propKey];

	if (!value || /^[a-zA-Z]*$/.test(value)) {
		return '';
	} else {
		return 'Value \'' + value + '\' is not alphabetic.';
	}
}

function positiveNumber(writer, propKey) {
	var value = writer.property[propKey];

	if (value && /^\+?([1-9]\d*)$/.test(value)) {
		return '';
	} else {
		return 'Value \'' + value + '\' is not positive number.';
	}
}

function alphanumeric(writer, propKey) {
	var value = writer.property[propKey];

	if (!value || /^\w+$/i.test(value)) {
		return '';
	} else {
		return 'Value \'' + value + '\' is not alphanumeric.';
	}
}

function mandatoryForUpsertOrUpdate(writer, propKey) {
	var writetype = writer.property["writetype"];

	if ('update' == writetype || 'upsert' == writetype) {
		var value = writer.property[propKey];

		if (!value) {
			return  'Value is not present for update or upsert operation.';
		}
	}
	return '';
}